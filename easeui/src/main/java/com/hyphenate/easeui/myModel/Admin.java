package com.hyphenate.easeui.myModel;

/**
 * @author duzq
 * @date 2020/07/22 14:10
 * 管理员信息
 **/
public class Admin {

    private long adminId;//主键ID
    private String nickName;//昵称
    private String account;//账号
    private String password;//密码
    private String email;//邮箱
    private String enable;//是否启用(0启用,2禁用,3未知)
    private String remark;//备注
    private int role;//角色(0超级管理员,1普通管理员)

    public long getAdminId() {
        return adminId;
    }

    public void setAdminId(long adminId) {
        this.adminId = adminId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
