package com.hyphenate.easeui.myModel;

/**
 * @author duzq
 * @date 2020/07/22 14:19
 * 公告
 **/
public class Notice {

    private long noticeId;//主键ID
    private Admin admin;//发布者
    private String title;//标题
    private String content;//内容
    private String createdDate;//发布时间

    public long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(long noticeId) {
        this.noticeId = noticeId;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


}
