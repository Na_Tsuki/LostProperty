package com.hyphenate.easeui.myAdapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hyphenate.easeui.R;
import com.hyphenate.easeui.myModel.Notice;

import java.util.List;

public class NoticeAdapter extends BaseAdapter {
    private List<Notice> list;

    private Context context;

    public NoticeAdapter(List<Notice> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.notice_item,parent,false);
        TextView tvId=view.findViewById(R.id.tvId);
        TextView tvTitle=view.findViewById(R.id.tvTitle);
        TextView tvContent=view.findViewById(R.id.tvContent);
        TextView tvName=view.findViewById(R.id.tvName);
        TextView tvCreated_date=view.findViewById(R.id.tvCreated_date);
        Notice notice=list.get(position);
        tvId.setText(""+notice.getNoticeId());
        tvTitle.setText(notice.getTitle());
        tvContent.setText(Html.fromHtml(notice.getContent()));
        tvName.setText(notice.getAdmin().getNickName());
        tvCreated_date.setText(notice.getCreatedDate());
        return view;
    }
}
