package com.hyphenate.easeui.myActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.hyphenate.easeui.R;

import java.io.InputStream;
import java.net.URL;


public class NoticeShowActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvContent, tvTitle;
    private Intent intent;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_show);
        init();

    }

    private void init() {
        ivBack = findViewById(R.id.ivBack);
        tvContent = findViewById(R.id.tvContent);
        tvTitle = findViewById(R.id.tvTitle);
        intent = getIntent();
        if (null != intent) {
            Bundle bundle = intent.getExtras();
            String title = null;
            String content = null;
            if (bundle != null) {
                title = bundle.getString("title");
                content = bundle.getString("content");
            }
            if (!"".equals(title) || title != null || !"".equals(content) || content != null) {
                tvTitle.setText(title);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    getNotice(content);
                } else {
                    tvContent.setText(Html.fromHtml(content));
                }
            } else {
                tvTitle.setText("");
                tvContent.setText("公告获取失败");
            }
        }
        ivBack.setOnClickListener(this);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0x101) {
                tvContent.setText((CharSequence) msg.obj);
            }
            super.handleMessage(msg);
        }
    };

    // 因为从网上下载图片是耗时操作 所以要开启新线程
    public void getNotice(final String content) {
        Thread t = new Thread(new Runnable() {
            Message msg = Message.obtain();

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void run() {
                /**
                 * 要实现图片的显示需要使用Html.fromHtml的一个重构方法：public static Spanned
                 * fromHtml (String source, Html.ImageGetterimageGetter,
                 * Html.TagHandler
                 * tagHandler)其中Html.ImageGetter是一个接口，我们要实现此接口，在它的getDrawable
                 * (String source)方法中返回图片的Drawable对象才可以。
                 */
                Html.ImageGetter imageGetter = new Html.ImageGetter() {
                    @Override
                    public Drawable getDrawable(String source) {
                        URL url;
                        Drawable drawable = null;
                        try {
                            url = new URL(source);
                            drawable = Drawable.createFromStream(
                                    url.openStream(), null);
                            drawable.setBounds(0, 0,
                                    drawable.getIntrinsicWidth(),
                                    drawable.getIntrinsicHeight());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return drawable;
                    }
                };
                CharSequence test = Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT, imageGetter, null);
                msg.what = 0x101;
                msg.obj = test;
                handler.sendMessage(msg);
            }
        });
        t.start();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivBack) {
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
