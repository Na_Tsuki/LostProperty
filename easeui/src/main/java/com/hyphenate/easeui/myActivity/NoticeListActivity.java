package com.hyphenate.easeui.myActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hyphenate.easeui.IpConfig;
import com.hyphenate.easeui.R;
import com.hyphenate.easeui.myAdapter.NoticeAdapter;
import com.hyphenate.easeui.myModel.Notice;
import com.hyphenate.easeui.myModel.Result;
import com.hyphenate.easeui.myUtils.HttpClientUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NoticeListActivity extends AppCompatActivity {
    private static final int NOTICE_ERROR = 1001;
    private static final int NOTICE_SUCCESS = 1002;
    private ListView lvNotice;
    private ProgressBar pbNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_list);
        init();
        getNoticeList();
    }

    private void init(){
        pbNotice=findViewById(R.id.pbNotice);
        lvNotice=findViewById(R.id.lvNotice);
        lvNotice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tvTitle=view.findViewById(R.id.tvTitle);
                TextView tvContent=view.findViewById(R.id.tvContent);
                Intent intent=new Intent(NoticeListActivity.this,NoticeShowActivity.class);
                intent.putExtra("title",tvTitle.getText().toString());
                intent.putExtra("content",tvContent.getText().toString());
                startActivity(intent);
            }
        });
    }

    Handler handler=new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case NOTICE_SUCCESS:
                    List<Notice> list= (List<Notice>) msg.obj;
                    listViewInit(list);
                    break;
                case NOTICE_ERROR:
                    Toast.makeText(NoticeListActivity.this, "公告获取失败", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void listViewInit(List<Notice> list){
        NoticeAdapter noticeAdapter=new NoticeAdapter(list,this);
        lvNotice.setAdapter(noticeAdapter);
        pbNotice.setVisibility(View.GONE);
    }

    private void getNoticeList(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message=new Message();
                Map<String,Object> map=new HashMap<String, Object>();
                map.put("requestType",1);
                String noticesJson= HttpClientUtils.HttpClientGetString(IpConfig.ROOTURL_Kittlen+"notice/selectAll",map,"","");
                if(!"".equals(noticesJson)){
                    Result result= Result.toResult(noticesJson);
                    if(result==null) {
                        message.what = NOTICE_ERROR;
                        handler.sendMessage(message);
                    }else{
                        Gson gson=new Gson();
                        String noteceList=result.getData().toString();
                        List<Notice> list=gson.fromJson(noteceList,new TypeToken<List<Notice>>(){}.getType());
                        message.what = NOTICE_SUCCESS;
                        message.obj=list;
                        handler.sendMessage(message);
                    }
                }else{
                    message.what=NOTICE_ERROR;
                    handler.sendMessage(message);
                }
            }
        }).start();
    }

}
