package com.example.lostproperty.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.Comments;
import com.example.lostproperty.model.LikeMap;
import com.example.lostproperty.model.User;
import com.example.lostproperty.utils.ImageLoaderUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class LikeAdapter extends BaseAdapter {
    private ImageLoader imageLoader= ImageLoader.getInstance();
    private Context context;
    private List<LikeMap> list;
    private String url = IpConfig.ROOTURL_tuanzili;

    public LikeAdapter(Context context, List<LikeMap> list) {
        this.context = context;
        this.list = list;
        ImageLoaderUtil.ImageLoaderInit(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        LikeMap likeMap = list.get(position);
        view = LayoutInflater.from(context).inflate(R.layout.item_likes, null);

        ImageView ivHead = view.findViewById(R.id.imageView13);
        TextView tvUsername=view.findViewById(R.id.textView20);
        TextView tvNickName=view.findViewById(R.id.textView21);

        User user=likeMap.getUser();
        String avatar=user.getAvatar();
        String phone=user.getPhone();
        tvNickName.setText(user.getNickName());
        tvUsername.setText(user.getUserName());
        String imgUrl=url+"upload/" + phone + "/" + avatar;
//        if(avatar==null){
//            ivHead.setImageResource(R.drawable.img3);
//        }else {
//            try {
//                imageLoader.displayImage(imgUrl,ivHead);
//            }catch (Exception e){
//                ivHead.setImageResource(R.drawable.img3);
//            }
//        }
        ivHead.setImageResource(R.drawable.ic_circle);

        return view;
    }
}
