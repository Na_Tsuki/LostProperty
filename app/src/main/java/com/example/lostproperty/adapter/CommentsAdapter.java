package com.example.lostproperty.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.Comments;
import com.example.lostproperty.model.User;
import com.example.lostproperty.utils.ImageLoaderUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class CommentsAdapter extends BaseAdapter {
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private Context context;
    private List<Comments> list;
    private String url = IpConfig.ROOTURL_tuanzili;

    public CommentsAdapter(Context context, List<Comments> list) {
        this.context = context;
        this.list = list;
        ImageLoaderUtil.ImageLoaderInit(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        Comments comment = list.get(position);
        view = LayoutInflater.from(context).inflate(R.layout.item_comments, null);

        ImageView ivHead = view.findViewById(R.id.imageView9);
        TextView tvNickName = view.findViewById(R.id.textView18);
        TextView tvComment = view.findViewById(R.id.textView19);
        TextView tvCreatedDate = view.findViewById(R.id.textView23);
        User user = comment.getUser();
        String avatar = user.getAvatar();
        String phone = user.getPhone();
        tvNickName.setText(user.getNickName());
        tvCreatedDate.setText(comment.getCreatedDate());
        tvComment.setText(comment.getContent());
//        String imgUrl = url + "upload/" + phone + "/" + avatar;
//
//        String uri = IpConfig.ROOTURL_IMAGE + phone + "/" + avatar;

        ivHead.setImageResource(R.drawable.ic_circle);


        return view;

    }
}
