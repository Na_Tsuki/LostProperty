package com.example.lostproperty.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.PushInfo;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PushInfoAdapter extends BaseAdapter {
    private ImageLoader imageLoader= ImageLoader.getInstance();
    private Context context;
    private List<PushInfo> list=new ArrayList<>();
    private Boolean androidimg=true;

    public PushInfoAdapter(Context context,List<PushInfo> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=null;
        PushInfo pushInfo=list.get(position);
        List<String> imglist=uploadImg(pushInfo.getContent());
        String Content=filterImg(pushInfo.getContent());
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        if(imglist.size()<=0) {
            view = LayoutInflater.from(context).inflate(R.layout.pushinfo_type_nomig_item, null);
            TextView lvTitle=view.findViewById(R.id.lvTitle);
            TextView lvContent=view.findViewById(R.id.lvContent);
            TextView lvLike_number=view.findViewById(R.id.lvLike_number);
            TextView lvNickname=view.findViewById(R.id.lvNickname);
            TextView lvCreated_date=view.findViewById(R.id.lvCreated_date);
            ImageView iv=view.findViewById(R.id.imageView);
            lvTitle.setText(pushInfo.getTitle());
            lvContent.setText(Html.fromHtml(pushInfo.getContent()));
            lvLike_number.setText("点赞："+pushInfo.getLikeNumber());
            lvNickname.setText(pushInfo.getUser().getNickName());
            lvCreated_date.setText(pushInfo.getCreatedDate());
        }else {
            view = LayoutInflater.from(context).inflate(R.layout.pushinfo_type_img_item, null);
            TextView lvTitle=view.findViewById(R.id.lvTitle);
            TextView lvContent=view.findViewById(R.id.lvContent);
            TextView lvLike_number=view.findViewById(R.id.lvLike_number);
            TextView lvNickname=view.findViewById(R.id.lvNickname);
            TextView lvCreated_date=view.findViewById(R.id.lvCreated_date);
            ImageView iv=view.findViewById(R.id.imageView);
            lvTitle.setText(pushInfo.getTitle());
            lvContent.setText(Html.fromHtml(Content));
            lvLike_number.setText("点赞："+pushInfo.getLikeNumber());
            lvNickname.setText(pushInfo.getUser().getNickName());
            lvCreated_date.setText(pushInfo.getCreatedDate());
            if(androidimg) {
                imageLoader.displayImage(IpConfig.ROOTURL_IMAGE + pushInfo.getUser().getUserName() + "/" + imglist.get(0), iv);
            }else {
                imageLoader.displayImage(imglist.get(0), iv);
            }
        }
        return view;
    }

    //从内容中检索图片
    private List<String> uploadImg(String content){
        List<String> uploadImg=new ArrayList<>();
        uploadImg=findUploadImg(content);
        if(uploadImg.size()<=0){
            uploadImg=findUploadwebImg(content);
            androidimg=false;
        }
        return uploadImg;
    }

    private List<String> findUploadImg(String content) {
        List<String> fileList=new ArrayList<>();
        String imgTag = "<img src=([\\u4E00-\\u9FA5A-Za-z0-9%_/.]*)/>";
        Pattern pattern = Pattern.compile(imgTag);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            String imgName = matcher.group(1);
            fileList.add(imgName);
        }
        androidimg=true;
        return fileList;
    }
    private List<String> findUploadwebImg(String content){
        List<String> pics = new ArrayList<String>();
        String img = "";
        Pattern p_image;
        Matcher m_image;
//        String regEx_img = ""; //图片链接地址
        String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
        p_image = Pattern.compile
                (regEx_img, Pattern.CASE_INSENSITIVE);
        m_image = p_image.matcher(content);
        while (m_image.find()) {
            // 得到<img />数据
            img = m_image.group();
            // 匹配<img>中的src数据
            Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
            while (m.find()) {
                String Img=m.group(1);
                pics.add(Img);
            }
        }
        return pics;
    }

    //内容加工
    private String filterImg(String content) {
        String str="";
        String imgTag = "<img.*>";


        Pattern pattern = Pattern.compile(imgTag);
        Matcher m = pattern.matcher(content);
        String result = m.replaceAll(str);
        return result;
    }
}