package com.example.lostproperty.utils;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.widget.Toast;

public class SwitchUtils {
    /**
     * GPS是否打开
     * @param context
     * @return
     */
    public static boolean getGpsStatus(Context context){
        LocationManager locationManager= (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    public static void checkGpsIsOpen(Context context,String hint){
        if(!getGpsStatus(context)){
            Toast.makeText(context,hint,Toast.LENGTH_SHORT);
            Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(intent);
        }
    }
}
