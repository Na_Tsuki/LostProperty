package com.example.lostproperty.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

public class DialogUtils {

    public static void dialog(Context context, String msg, String title, DialogInterface.OnClickListener l) {
      AlertDialog.Builder builder = new AlertDialog.Builder(context);
      builder.setMessage(msg);
      builder.setTitle(title);
      builder.setPositiveButton("确认", l);
      builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
       @Override
       public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
       }
      });
      builder.create().show();
     }

    public static void dialog2(Context context, String title, final View DialogView, DialogInterface.OnClickListener l) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setView(DialogView);
        builder.setPositiveButton("确认", l);
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


}
