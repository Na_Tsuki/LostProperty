package com.example.lostproperty.utils;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class SharedPreferencesUtils {
    //定义一个SharePreference对象
    SharedPreferences sharedPreferences;
    //定义一个上下文对象

    //创建SharePreference对象时要上下文和存储的模式
    //通过构造方法传入一个上下文
    public SharedPreferencesUtils(Context context, String fileName) {
        //实例化SharePreference对象，使用的是get方法，而不是new创建
        //第一个参数是文件的名字
        //第二个参数是存储的模式，一般都是使用私有方式：Context.MODE_PRIVATE
        sharedPreferences = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    //创建一个内部类使用，里面有key和value这两个值
    public static class ContentValue {
        String key;
        Object value;

        //通过构造方法来传入key和value
        public ContentValue(String key, Object value) {
            this.key = key;
            this.value = value;
        }
    }

    //一次可以传入多个ContentValue对象的值
    public void putValues(ContentValue... contentValues) {
        //获取SharePreference对象的编辑对象，才能进行数据的存储
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //数据分类和存储
        for (ContentValue contentValue : contentValues) {
            //如果是字符型类型
            if (contentValue.value instanceof String) {
                editor.putString(contentValue.key, contentValue.value.toString()).commit();
            }
            //如果是int类型
            if (contentValue.value instanceof Integer) {
                editor.putInt(contentValue.key, Integer.parseInt(contentValue.value.toString())).commit();
            }
            //如果是Long类型
            if (contentValue.value instanceof Long) {
                editor.putLong(contentValue.key, Long.parseLong(contentValue.value.toString())).commit();
            }
            //如果是布尔类型
            if (contentValue.value instanceof Boolean) {
                editor.putBoolean(contentValue.key, Boolean.parseBoolean(contentValue.value.toString())).commit();
            }

        }
    }


    //获取数据
    public String getString(String key) {
        return sharedPreferences.getString(key, null);
    }

    public boolean getBoolean(String key, Boolean b) {
        return sharedPreferences.getBoolean(key, b);
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, -1);
    }

    public long getLong(String key) {
        return sharedPreferences.getLong(key, -1);
    }

    //清除当前文件的所有的数据
    public void clear() {
        sharedPreferences.edit().clear().commit();
    }

    /**
     * 获取name的SharedPreferences存储的数据
     * @param context
     * @param name 存储名
     * @param key 获取的键
     * @return map<String,String>
     */
    public static Map<String,String> getData(Context context, String name, List<String> key){
        SharedPreferences sp=context.getSharedPreferences(name,MODE_PRIVATE);
        Map<String,String> map=new HashMap<>();
        for(int i=0;i<key.size();i++){
            map.put(key.get(i),sp.getString(key.get(i),""));
        }
        return map;
    }


    /**
     * 获取name的SharedPreferences存储的数据
     * @param context
     * @param name 存储名
     * @param key 获取的键
     * @return map<String,String>
     */
    public static String getData(Context context,String name,String key){
        SharedPreferences sp=context.getSharedPreferences(name,MODE_PRIVATE);
        return sp.getString(key,"");
    }

    /**
     * SharedPreferences存储 名称为name
     * @param context
     * @param name 存储名
     * @param data 数据<map>
     */
    public static void putData(Context context,String name,Map<String,String> data){
        SharedPreferences sp=context.getSharedPreferences(name,MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        for(Map.Entry<String,String> entry:data.entrySet()){
            editor.putString(entry.getKey(),entry.getValue());
        }
        editor.commit();
    }

    /**
     * 清空SharedPreferences
     * @param context
     * @param name 存储名
     */
    public static void clear(Context context,String name){
        SharedPreferences sp=context.getSharedPreferences(name,MODE_PRIVATE);
        sp.edit().clear().commit();//删除数据
    }

}