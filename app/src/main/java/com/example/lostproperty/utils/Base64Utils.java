package com.example.lostproperty.utils;

import android.util.Base64;

public class Base64Utils {


    /**
     * BASE64解密
     */
    public static String decryptBASE64(String key) {
        int decodetime = 5;//解码次数
        byte[] bt;
        key = key.trim().replace(" ", "");//去掉空格
        while (decodetime > 0) {
            bt = Base64.decode(key.getBytes(), Base64.DEFAULT);
            key = new String(bt);
            decodetime--;
        }
        return key;
    }

    /**
     * BASE64加密
     */
    public static String encryptBASE64(String key) {
        int decodetime = 5;//解码次数
        byte[] bt = null;
        key = key.trim().replace(" ", "");//去掉空格
        while (decodetime > 0) {
            bt = key.getBytes();
            key = Base64.encodeToString(bt, Base64.DEFAULT);
            decodetime--;
        }

        return key;
    }
}
