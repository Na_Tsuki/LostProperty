package com.example.lostproperty.utils;

/**
 * @author duzq
 * @date 2020-07-25 10:39
 **/
public class Constant {

    //打开用户收藏界面
    public static final int MYLOVE_MESSAGE = 1001;

    //关闭用户收藏界面
    public static final int MYLOVE_RESULT_MESSAGE = 10001;

    //打开用户帖子界面
    public static final int MYPUSH_MESSAGE = 1002;

    //关闭用户帖子界面
    public static final int MYPUSH_RESULT_MESSAGE = 10002;

    //请求用户的收藏(成功)
    public static final int GET_MYLOVE_MESSAGE_SUCCESS = 1003;

    //请求用户的收藏(失败)
    public static final int GET_MYLOVE_MESSAGE_FAIL = 10003;

    //请求用户的帖子(成功)
    public static final int GET_MYPUSH_MESSAGE_SUCCESS  = 1004;

    //请求用户的帖子(失败)
    public static final int GET_MYPUSH_MESSAGE_FAIL = 10004;

    //查看帖子详情(成功)
    public static final int PUSHINFO_CONTENT_MESSAGE_SUCCESS  = 1005;

    //查看帖子详情(失败)
    public static final int PUSHINFO_CONTENT_MESSAGE_FAIL  = 10005;

    //删除个人帖子(成功)
    public static final int PUSHINFO_DELETE_MESSAGE_SUCCESS  = 1006;

    //删除个人帖子(失败)
    public static final int PUSHINFO_DELETE_MESSAGE_FAIL  = 10006;

    //打开搜索界面
    public static final int FINDSEARCH_MESSAGE = 1007;

    //关闭搜索界面
    public static final int FINDSEARCH_RESULT_MESSAGE = 10007;

    //搜索对应的帖子(成功)
    public static final int FINDSEARCH_MESSAGE_SUCCESS  = 1008;

    //搜索对应的帖子(失败)
    public static final int FINDSEARCH_MESSAGE_FAIL  = 10008;

    //请求失败
    public static final int GET_REQUEST_FAIL  = 9999;




}
