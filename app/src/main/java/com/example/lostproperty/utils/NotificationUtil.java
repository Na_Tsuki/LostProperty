package com.example.lostproperty.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.RequiresApi;

import com.example.lostproperty.R;

public class NotificationUtil {

    private  static String NOTIFICATIONCHANNEL_ID="foreground";
    private static  String NOTIFICATIONCHANNEL_NAME="foregroundName";



    //A组件 创建了一个 PendingIntent 的对象然后传给 B组件，B 在执行这个 PendingIntent 的 send 时候，它里面的 Intent 会被发送出去，而接受到这个 Intent 的 C 组件会认为是 A 发的。
    //B以A的权限和身份发送了这个Intent
    //创建一个跳转道活动页面的意图
    //Intent intent=new Intent(context,MainActivity.class);
    //创建一个用于页面跳转的延迟意图
    //PendingIntent pendingIntent=PendingIntent.getActivity(context,R.string.app_name,intent,PendingIntent.FLAG_UPDATE_CURRENT);




    /**发送简单的通知消息(包括消息标题和消息内容)
     *
     * @param ticker 状态栏提示文本
     * @param title 标题
     * @param subText 标题后的文本
     * @param message 内容
     * @param contentIntent 跳转的延迟意图
     * @param icon 消息图标
     * @param time 推送时间
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void sendSimpleNotify(Context context, String ticker, String title, String subText, String message, PendingIntent contentIntent, @DrawableRes int icon, long time){

        //创建一个通知消息的构造器
        Notification.Builder builder=getNotificationBuiler(context,ticker,title,subText,message,contentIntent,icon,time);
        NotificationManager manager= (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);//从系统服务中获取通知栏管理器
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //这里将刚才的channel id传入
            builder.setChannelId(NOTIFICATIONCHANNEL_ID);
            NotificationChannel channel = new NotificationChannel(NOTIFICATIONCHANNEL_ID, NOTIFICATIONCHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
//            //8.0以上取消振动,需要重装应用
//            channel.enableVibration(false);
//            channel.setVibrationPattern(new long[]{0});
//            /*******************/
            manager.createNotificationChannel(channel);
        }
        Notification notification=builder.build();//根据消息构建器构建一个通知对象
         manager.notify(R.string.app_name,notification);//使用通知管理器推送通知,然后在手机通知栏就会看到该消息
    }

    /**在当前时间发送一条通知消息
     *
     * @param ticker 状态栏提示文本
     * @param title 标题
     * @param subText 标题后的文本
     * @param message 内容
     * @param contentIntent 跳转的延迟意图
     * @param icon 消息图标
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void sendSimpleNotify(Context context, String ticker, String title,String subText, String message, PendingIntent contentIntent, @DrawableRes int icon){
        sendSimpleNotify(context,ticker,title, subText,message, contentIntent,  icon ,System.currentTimeMillis());
    }

    /**
     *
     * @param context
     * @param ticker 状态栏里面的提示文本
     * @param title 通知栏里面的标题文本
     * @param subText 通知栏应用名后面的文本
     * @param message 通知栏里面的内容文本
     * @param contentIntent 内容的点击意图
     * @param icon 状态栏里的图标
     * @param time 推送时间
     * @param ProgressMax 进度条最大值
     * @param Progress 进度条当前值
     * @param indeterminate 进度提状态 false 具体状态 true 不确定状态
     */
    public static void sendCounterNotify(Context context, String ticker, String title, String subText, String message, PendingIntent contentIntent, @DrawableRes int icon, long time, int ProgressMax, int Progress , boolean indeterminate){
        Notification.Builder builder=getNotificationBuiler(context,ticker,title,subText,message,contentIntent,icon,time);
        //        builder.setVibrate(new long[]{0});//8.0一下取消振动
        builder.setProgress(ProgressMax,Progress,indeterminate);//设置进度条与当前进度 设置为true标示不确定
        NotificationManager manager= (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);//从系统服务中获取通知栏管理器
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //这里将刚才的channel id传入
            builder.setChannelId(NOTIFICATIONCHANNEL_ID);
            NotificationChannel channel = new NotificationChannel(NOTIFICATIONCHANNEL_ID, NOTIFICATIONCHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
//            //8.0以上取消振动,需要重装应用
//            channel.enableVibration(false);
//            channel.setVibrationPattern(new long[]{0});
//            /*******************/
            manager.createNotificationChannel(channel);
        }
        Notification notification=builder.build();//根据消息构建器构建一个通知对象
        manager.notify(R.string.app_name,notification);//使用通知管理器推送通知,然后在手机通知栏就会看到该消息
    }

    /**
     *
     * @param context
     * @param ticker 状态栏里面的提示文本
     * @param title 通知栏里面的标题文本
     * @param subText 通知栏应用名后面的文本
     * @param message 通知栏里面的内容文本
     * @param contentIntent 内容的点击意图
     * @param icon 状态栏里的图标
     * @param ProgressMax 进度条最大值
     * @param Progress 进度条当前值
     * @param indeterminate 进度提状态 false 具体状态 true 不确定状态
     */
    public static void sendCounterNotify(Context context, String ticker, String title, String subText, String message, PendingIntent contentIntent, @DrawableRes int icon, int ProgressMax, int Progress, boolean indeterminate){
        sendCounterNotify(context,ticker,title,subText, message, contentIntent,  icon ,System.currentTimeMillis(),ProgressMax,Progress,indeterminate);
    }





    /**
     * 返回一个基本Notification.Builder对象
     * @param context
     * @param ticker 状态栏里面的提示文本
     * @param title 通知栏里面的标题文本
     * @param subText 通知栏应用名后面的文本
     * @param message 通知栏里面的内容文本
     * @param contentIntent 内容的点击意图
     * @param icon 状态栏里的图标
     * @param time 推送时间
     * @return Notification.Builder对象
     */
    public static Notification.Builder getNotificationBuiler(Context context, String ticker, String title, String subText, String message, PendingIntent contentIntent, @DrawableRes int icon, long time){
        Notification.Builder builder=new Notification.Builder(context);
        builder.setContentIntent(contentIntent);//设置内容的点击意图
        builder.setAutoCancel(true);//设置是否允许自动消除
        builder.setSmallIcon(icon);//设置状态栏里的小图标
        builder.setTicker(ticker);//设置状态栏里面的提示文本
        builder.setWhen(time);//设置推送时间
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(),icon));//设置通知栏里的大图标
        builder.setContentTitle(title);//设置通知栏里面的标题文本
        builder.setSubText(subText);//设置通知栏应用名后面的文本
        builder.setContentText(message);//设置通知栏里面的内容文本
        return builder;
    }

    /**
     * 自定义通知栏消息
     * @param context
     * @param layoutResID 自定义的layout
     * @param icon 状态栏图标(必须)
     * @param contentIntent 点击意图
     */
    public static void sendCustomNotify(Context context, @LayoutRes int layoutResID , @DrawableRes int icon, PendingIntent contentIntent){
        RemoteViews remoteViews=new RemoteViews(context.getPackageName(),layoutResID);
        Notification.Builder builder=new Notification.Builder(context);
//        builder.setVibrate(new long[]{0});//8.0一下取消振动
        NotificationManager manager= (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);//从系统服务中获取通知栏管理器
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //这里将刚才的channel id传入
            builder.setChannelId(NOTIFICATIONCHANNEL_ID);
            NotificationChannel channel = new NotificationChannel(NOTIFICATIONCHANNEL_ID, NOTIFICATIONCHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
//            //8.0以上取消振动,需要重装应用
//            channel.enableVibration(false);
//            channel.setVibrationPattern(new long[]{0});
            /*******************/
            manager.createNotificationChannel(channel);
        }
        builder.setContent(remoteViews);
        builder.setContentIntent(contentIntent);//设置内容的点击意图
        builder.setSmallIcon(icon);//设置状态栏里的小图标(必须)
        Notification notification=builder.build();//根据消息构建器构建一个通知对象
        manager.notify(R.string.app_name,notification);//使用通知管理器推送通知,然后在手机通知栏就会看到该消息
    }


}
