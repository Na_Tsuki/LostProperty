package com.example.lostproperty.baidumap;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSONObject;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.example.lostproperty.R;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.Permissions;
import com.example.lostproperty.utils.SwitchUtils;

public class BaiduMapActivity extends AppCompatActivity implements View.OnClickListener {

    private MapView mMapView;
    private BaiduMap mBaiduMap;
    private LocationClient mLocationClient;
    private boolean isFirstLoc = true;
    private LatLng ll = null;
    private TextView textmap;
    private double longitude1;
    private double latitude1;
    private TextView mapback;
    private TextView mapenter;

    private MyLocationListener myLocationListenery = new MyLocationListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baidumap);
        mMapView = findViewById(R.id.mapView);
        textmap = findViewById(R.id.textmap);
        mapback = findViewById(R.id.mapback);
        mapenter = findViewById(R.id.mapenter);
        Permissions.verifyLocationPermissions(this);
        SwitchUtils.checkGpsIsOpen(this, "请打开GPS");

        mLocationClient = new LocationClient(this);
        mBaiduMap = mMapView.getMap();

        mapback.setOnClickListener(this);
        mapenter.setOnClickListener(this);

        initMap();


    }



    private void initMap() {
        //获取地图控件引用
        mBaiduMap = mMapView.getMap();
        //普通地图
        mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        // 开启定位图层
        mBaiduMap.setMyLocationEnabled(true);
        //声明LocationClient类
        mLocationClient = new LocationClient(getApplicationContext());
        //配置定位SDK参数
        initLocation();
        //注册监听函数
        mLocationClient.registerLocationListener(myLocationListenery);
        //开启定位
        mLocationClient.start();
        myLocationListenery.mapClick();
    }

    //配置定位SDK参数
    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        //可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        //可选，默认gcj02，设置返回的定位结果坐标系
        option.setCoorType("bd09ll");
        //可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        int span = 0;
        option.setScanSpan(span);
        //可选，设置是否需要地址信息，默认不需要
        option.setIsNeedAddress(true);
        //可选，默认false,设置是否使用gps
        option.setOpenGps(true);
        //可选，默认false，设置是否当GPS有效时按照1S/1次频率输出GPS结果
        option.setLocationNotify(true);
        //可选，默认false，设置是否需要位置语义化结果，可以在BDLocation
        option.setIsNeedLocationDescribe(true);
        // .getLocationDescribe里得到，结果类似于“在北京天安门附近”
        //可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIsNeedLocationPoiList(true);
        option.setIgnoreKillProcess(false);
        // 打开gps
        option.setOpenGps(true);
        //可选，默认false，设置是否收集CRASH信息，默认收集
        option.SetIgnoreCacheException(false);
        //可选，默认false，设置是否需要过滤GPS仿真结果，默认需要
        option.setEnableSimulateGps(true);
        mLocationClient.setLocOption(option);
    }




    public class MyLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            // 构造定位数据
            MyLocationData locData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                    // 此处设置开发者获取到的方向信息，顺时针0-360
                    .direction(0).latitude(location.getLatitude()).longitude(location.getLongitude())
                    .build();
            // 设置定位数据
            mBaiduMap.setMyLocationData(locData);
            if (isFirstLoc) {
                isFirstLoc = false;
                ll = new LatLng(location.getLatitude(),location.getLongitude());
                MapStatus.Builder builder = new MapStatus.Builder();
                builder.target(ll).zoom(16.0f);

                mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));

                if (location.getLocType() == BDLocation.TypeGpsLocation) {
                    // GPS定位结果
                    Toast.makeText(BaiduMapActivity.this, location.getAddrStr(), Toast.LENGTH_SHORT).show();
                }
            }

            String addr = location.getAddrStr();    //获取详细地址信息
            String country = location.getCountry();    //获取国家
            String province = location.getProvince();    //获取省份
            String city = location.getCity();    //获取城市
            String district = location.getDistrict();    //获取区县
            String street = location.getStreet();    //获取街道信息
            String adcode = location.getAdCode();    //获取adcode
            String town = location.getTown();    //获取乡镇信息
            System.out.println(province+city+district+street+town);

            textmap.setText(province+city+district+street+town);
        }

        private void addMarker(LatLng latLng) {
            mBaiduMap.clear();
//构建Marker图标
            BitmapDescriptor bitmap = BitmapDescriptorFactory
                    .fromResource(R.drawable.lulu);
//构建MarkerOption，用于在地图上添加Marker
            OverlayOptions option = new MarkerOptions()
                    .position(latLng) //必传参数
                    .icon(bitmap) //必传参数
                    .draggable(true)
//设置平贴地图，在地图中双指下拉查看效果
                    .flat(true)
                    .perspective(true);
//在地图上添加Marker，并显示
            mBaiduMap.addOverlay(option);
//            maploc(latLng);



        }
        private void mapClick(){
            final BaiduMap.OnMapClickListener listener = new BaiduMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng point) {
                    addMarker(point);
                    latitude1 = point.latitude;
                    longitude1 = point.longitude;
                    Log.i("aaa",String.valueOf(latitude1));
                    Log.i("aaa",String.valueOf(longitude1));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            String url = "http://api.map.baidu.com/cloudrgc/v1?geotable_id=8396953&&ak=fDb9hmtLGcgdtgm7zCBiQgdeuTjqp6lp&coord_type=bd09ll&location="+latitude1+","+longitude1;
                            try {
                                String s = HttpClientUtils.HttpClientGet(url, null);
                                JSONObject jsonObj = JSONObject.parseObject(s);
                                String b = jsonObj.get("formatted_address").toString();
                                String o = jsonObj.get("recommended_location_description").toString();
                                textmap.setText(b+o);
                                Log.i("aaa",b+o);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();


                }

                @Override
                public void onMapPoiClick(MapPoi mapPoi) {

                }

            };
            mBaiduMap.setOnMapClickListener(listener);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.mapenter:
                Intent intent = new Intent();
                String str = textmap.getText().toString();
                intent.putExtra("mapmsg",textmap.getText());
                setResult(911, intent);
                finish();
                break;
            case R.id.mapback:
                finish();
                break;
        }
    }
}
