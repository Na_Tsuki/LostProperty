package com.example.lostproperty.mainfragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.ui.ChangePWDActivity;
import com.example.lostproperty.ui.MainActivity;
import com.example.lostproperty.ui.MyLoveActivity;
import com.example.lostproperty.ui.MyPushActivity;
import com.example.lostproperty.ui.MyselfActivity;
import com.example.lostproperty.ui.UserLoginActivity;
import com.example.lostproperty.utils.Constant;
import com.example.lostproperty.utils.ImageLoaderUtil;
import com.example.lostproperty.utils.RoundImageView;
import com.example.lostproperty.utils.SharedPreferencesUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MyselfFragment extends Fragment implements View.OnClickListener {
    private RoundImageView mHead;
    private View mInformation;
    private TextView tvUsername;
    private TextView mSex;
    private TextView mPhone;
    private View updatePassword;
    private View myself;
    private TextView tvmylove;//我的收藏
    private TextView tvmypush;//我的帖子
    private TextView tvLogout;
    private TextView tvExitApp;
    private boolean isloging = false;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private String Url = IpConfig.ROOTURL_YEN_LUY + "/upload/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myself, container, false);
        initViews(view);
        setupEvents();
        setMyself();
        return view;

    }

    private void setMyself() {
        SharedPreferences spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        String spUserName = spUser.getString("username", "");

        if (!spUserName.equals("")) {
            isloging = true;
            String NickName = spUser.getString("nickname", "");
            String UserPhone = spUser.getString("phone", "");
            String UserAvatar = spUser.getString("avatar", "123");
            int gender = spUser.getInt("gender", 0);
            String strGender = "未知";
            if (gender == 1) {
                strGender = "男";
            } else if (gender == 2) {
                strGender = "女";
            }
            tvUsername.setText(NickName);
            mPhone.setText(UserPhone);
            mSex.setText(strGender);
            ImageLoaderUtil.ImageLoaderInit(getActivity());
            String uri=IpConfig.ROOTURL_IMAGE + UserPhone + "/" + UserAvatar;
            if (UserAvatar.equals("123")) {
                mHead.setImageResource(R.drawable.appicon);
            } else {
                imageLoader.displayImage(uri, mHead);
            }
        }
    }

    private void initViews(View v) {
        mHead = v.findViewById(R.id.userhead);
        mInformation = v.findViewById(R.id.userinformation);
        mSex = v.findViewById(R.id.myselfsex);
        tvUsername = v.findViewById(R.id.myselfusername);
        mPhone = v.findViewById(R.id.myselfphone);
        updatePassword = v.findViewById(R.id.uppwd);
        myself = v.findViewById(R.id.myself);
        tvmylove = v.findViewById(R.id.tvmylove);
        tvmypush = v.findViewById(R.id.tvmypush);
        tvLogout = v.findViewById(R.id.logout);
        tvExitApp = v.findViewById(R.id.exitapp);

    }

    private void setupEvents() {
        mHead.setOnClickListener(this);
        mInformation.setOnClickListener(this);
        updatePassword.setOnClickListener(this);
        tvmypush.setOnClickListener(this);
        myself.setOnClickListener(this);
        tvmylove.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        tvExitApp.setOnClickListener(this);
        tvmylove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyLoveActivity.class);
                startActivityForResult(intent, Constant.MYLOVE_MESSAGE);
            }
        });

        tvmypush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyPushActivity.class);
                startActivityForResult(intent, Constant.MYPUSH_MESSAGE);
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvmylove:
                Intent myLoveIntent = new Intent(getActivity(), MyLoveActivity.class);
                startActivity(myLoveIntent);
                break;
            case R.id.tvmypush:
                Intent myPushIntent = new Intent(getActivity(), MyPushActivity.class);
                startActivity(myPushIntent);
                break;
            case R.id.userhead:
            case R.id.userinformation:
            case R.id.myself:
                if (isloging == false) {
                    Intent loginIntent = new Intent(getActivity(), UserLoginActivity.class);
                    startActivity(loginIntent);
                    getActivity().finish();
                } else {
                    Intent mySelfIntent = new Intent(getActivity(), MyselfActivity.class);
                    startActivity(mySelfIntent);
                    getActivity().finish();
                }
                break;
            case R.id.uppwd:
                if (isloging == false) {
                    Intent loginIntent = new Intent(getActivity(), UserLoginActivity.class);
                    startActivity(loginIntent);
                    getActivity().finish();
                } else {
                    Intent mySelfIntent = new Intent(getActivity(), ChangePWDActivity.class);
                    startActivity(mySelfIntent);
                    getActivity().finish();
                }
                break;
            case R.id.logout:
                SharedPreferences spUser = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                spUser.edit().clear().commit();
                SharedPreferencesUtils helper = new SharedPreferencesUtils(getActivity(), "setting");
                helper.putValues(new SharedPreferencesUtils.ContentValue("autoLogin", false));
                MainActivity activity= (MainActivity) getActivity();
                activity.reLoadFragView();
                break;
            case R.id.exitapp:
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.MYLOVE_MESSAGE) {
            if (resultCode == Constant.MYLOVE_RESULT_MESSAGE) {

            }
        }
        if (requestCode == Constant.MYPUSH_MESSAGE) {
            if (resultCode == Constant.MYPUSH_RESULT_MESSAGE) {

            }
        }
    }

}
