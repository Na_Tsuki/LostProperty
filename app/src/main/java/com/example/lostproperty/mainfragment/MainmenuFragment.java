package com.example.lostproperty.mainfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.lostproperty.R;
import com.example.lostproperty.typefragment.Findall;
import com.example.lostproperty.typefragment.Findlost;
import com.example.lostproperty.typefragment.Findloster;


public class MainmenuFragment extends Fragment implements View.OnClickListener {

    private TextView findall;
    private TextView findlost;
    private TextView findloster;

    private Findall mfindall;
    private Findlost mfindlost;
    private Findloster mfindloster;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mainmenu, container, false);

        findall = view.findViewById(R.id.findall);
        findlost = view.findViewById(R.id.findlost);
        findloster = view.findViewById(R.id.findloster);


        findall.setOnClickListener(this);
        findlost.setOnClickListener(this);
        findloster.setOnClickListener(this);

        mfindall = new Findall();
        mfindlost = new Findlost();
        mfindloster = new Findloster();


        getChildFragmentManager().beginTransaction().replace(R.id.mainflt,mfindall).commit();

        return view;

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.findall:
                if(mfindall==null){
                    mfindall=new Findall();
                }
                getChildFragmentManager().beginTransaction().replace(R.id.mainflt, mfindall).commit();
                break;
            case R.id.findlost:
                if(mfindlost==null){
                    mfindlost=new Findlost();
                }
                getChildFragmentManager().beginTransaction().replace(R.id.mainflt, mfindlost).commit();
                break;
            case R.id.findloster:
                if(mfindloster==null){
                    mfindloster=new Findloster();
                }
                getChildFragmentManager().beginTransaction().replace(R.id.mainflt, mfindloster).commit();
                break;

        }
    }
}
