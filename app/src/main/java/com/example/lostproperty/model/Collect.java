package com.example.lostproperty.model;

/**
 * @author duzq
 * @date 2020/07/22 14:14
 * 用户收藏
 **/
public class Collect {

    private long collectId;//主键ID
    private User user;//用户
    private PushInfo pushInfo;//发布消息
    private String createdDate;//创建时间

    public long getCollectId() {
        return collectId;
    }

    public void setCollectId(long collectId) {
        this.collectId = collectId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PushInfo getPushInfo() {
        return pushInfo;
    }

    public void setPushInfo(PushInfo pushInfo) {
        this.pushInfo = pushInfo;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
