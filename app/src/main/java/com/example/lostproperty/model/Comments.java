package com.example.lostproperty.model;

/**
 * @author duzq
 * @date 2020/07/22 14:17
 * 用户评论
 **/
public class Comments {

    private long commentsId;//主键ID
    private User user;//用户
    private String content;//内容
    private PushInfo pushInfo;//发布消息
    private String createdDate;//评论时间


    public long getCommentsId() {
        return commentsId;
    }

    public void setCommentsId(long commentsId) {
        this.commentsId = commentsId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PushInfo getPushInfo() {
        return pushInfo;
    }

    public void setPushInfo(PushInfo pushInfo) {
        this.pushInfo = pushInfo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
