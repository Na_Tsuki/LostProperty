package com.example.lostproperty.model;

/**
 * @author duzq
 * @date 2020/07/22 14:18
 * 用户消息
 **/
public class Msg {

    private long msgId;//主键ID
    private User user;//用户
    private String content;//内容

    public long getMsgId() {
        return msgId;
    }

    public void setMsgId(long msgId) {
        this.msgId = msgId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
