package com.example.lostproperty.model;

public class LikeMap {
    private long likeMapId;//主键ID
    private PushInfo pushInfo;//发布消息id
    private User user;//用户id
    private String createdDate;//创建时间

    public long getLikeMapId() {
        return likeMapId;
    }

    public void setLikeMapId(long likeMapId) {
        this.likeMapId = likeMapId;
    }

    public PushInfo getPushInfo() {
        return pushInfo;
    }

    public void setPushInfo(PushInfo pushInfo) {
        this.pushInfo = pushInfo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
