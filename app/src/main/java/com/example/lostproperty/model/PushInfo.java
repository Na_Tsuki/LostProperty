package com.example.lostproperty.model;

/**
 * @author duzq
 * @date 2020/07/22 14:51
 * 发布信息
 **/
public class PushInfo {

    private long pushInfoId;//主键ID
    private User user;//用户
    private String title;//标题
    private String content;//内容
    private String address;//地址
    private String phone;//电话
    private int lastType;//丢失东西/找到东西(0丢失,1找到)
    private String createdDate;//创建时间
    private int status;//审核状态(0审核通过,1审核不通过,2未审核)
    private Type type;//发布的类型
    private int likeNumber;

    public int getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber = likeNumber;
    }

    public long getPushInfoId() {
        return pushInfoId;
    }

    public void setPushInfoId(long pushInfoId) {
        this.pushInfoId = pushInfoId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getLastType() {
        return lastType;
    }

    public void setLastType(int lastType) {
        this.lastType = lastType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
