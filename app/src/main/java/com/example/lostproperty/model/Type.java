package com.example.lostproperty.model;

/**
 * @author duzq
 * @date 2020/07/22 14:13
 * 失物/拾物类目
 **/
public class Type {

    private long typeId;//主键Id
    private String name;//类型名称
    private Admin admin;//添加者

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

}
