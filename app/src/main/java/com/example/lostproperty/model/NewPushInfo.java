package com.example.lostproperty.model;

/**
 * @author duzq
 * @date 2020/07/22 14:40
 * 最新(未推送)评论
 **/
public class NewPushInfo {

    private long newPushInfoId;//主键ID
    private User user;//被评论的用户
    private PushInfo pushInfo;//发布的消息
    private int size;//数量

    public long getNewPushInfoId() {
        return newPushInfoId;
    }

    public void setNewPushInfoId(long newPushInfoId) {
        this.newPushInfoId = newPushInfoId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PushInfo getPushInfo() {
        return pushInfo;
    }

    public void setPushInfo(PushInfo pushInfo) {
        this.pushInfo = pushInfo;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
