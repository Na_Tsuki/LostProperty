package com.example.lostproperty.model;

/**
 * @author duzq
 * @date 2020/07/22 14:10
 *管理员审核
 **/
public class AdminPushMap {

    private long adminPushMapId;//主键ID
    private PushInfo pushInfo;//发布消息ID
    private int status;//审核状态(0未审核,1审核通过,2审核不通过)
    private Admin admin;//管理员Id
    private String createdDate;//创建时间

    public long getAdminPushMapId() {
        return adminPushMapId;
    }

    public void setAdminPushMapId(long adminPushMapId) {
        this.adminPushMapId = adminPushMapId;
    }

    public PushInfo getPushInfo() {
        return pushInfo;
    }

    public void setPushInfo(PushInfo pushInfo) {
        this.pushInfo = pushInfo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
