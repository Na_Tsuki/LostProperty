package com.example.lostproperty.model;

import android.renderscript.Sampler;

/**
 * @author duzq
 * @date 2020/07/22 14:10
 * 用户信息
 **/
public class User{

    private long userId;//主键ID
    private String userName;//用户名
    private String password;//密码
    private String nickName;//昵称
    private String avatar;//头像
    private String phone;//手机号
    private int gender;//性别(0未知,1男,2女)
    private String createdDate;//创建时间

    public long getUserId() {
        return userId;
    }
    public String getStrUserId() {
        String strUserid=userId+"";
        return strUserid;
    }
    public void setStrUserId(String userId) {
        this.userId = Long.valueOf(userId) ;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


}
