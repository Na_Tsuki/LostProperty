package com.example.lostproperty.typefragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.adapter.PushInfoAdapter;
import com.example.lostproperty.model.PushInfo;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.ui.PropertyActivity;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.ListLoadUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Findall extends Fragment {
    private static final int SELECT_PUSHINFO_RESULT = 1011;
    private static final int SHOW_PUSHINFO = 1011;
    private static final int SELECT_PUSHINFO_RESULT_ERROR = 1010;
    private ProgressBar pbFindAll;
    private ListLoadUtil lvFindAll;
    private Context context;
    private long max=0;
    private int index;
    private Boolean lock=false;
    public List<PushInfo>pushinfolist=new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_findall, container, false);
        pbFindAll=view.findViewById(R.id.pbFindAll);
        lvFindAll=view.findViewById(R.id.lvFindAll);
        context=this.getContext();
        pushinfolist.clear();
        index=0;

        getPushInfoThread();
        return view;
    }


    private Handler handler=new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case SELECT_PUSHINFO_RESULT:
                    List<PushInfo> list= (List<PushInfo>) msg.obj;
                    pushinfolist.addAll(list);
                    if(list==null||list.size()==0){
                        Toast.makeText(context, "未获取到任何新闻", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    lock=true;
                    setFragmentView(pushinfolist);
                    break;
                case SELECT_PUSHINFO_RESULT_ERROR:
                    Toast.makeText(context, "获取新闻列表失败", Toast.LENGTH_SHORT).show();
                    if(pbFindAll==null){
                        pbFindAll=getView().findViewById(R.id.pbFindAll);
                    }
                    pbFindAll.setVisibility(View.GONE);
                    break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPushInfoThread();
    }

    //获取发布列表
    private void getPushInfoThread(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson=new Gson();
                Message message = new Message();
                List<PushInfo> list=new ArrayList<>();
                Map<String,Object> map=new HashMap<>();
                index++;
                map.put("index",index);
                String listStr= null;
                try {
                    listStr = HttpClientUtils.HttpClientGet(IpConfig.ROOTURL_PUSHINFO+"obtainPushInfo?status=0&lastType=-1&page="+index+"&limit=6",map);
                    Result result= gson.fromJson(listStr, Result.class);
                    if(result==null) {
                        message.what = SELECT_PUSHINFO_RESULT_ERROR;
                        handler.sendMessage(message);
                    }
                    max=result.getCount();
                    String Str=result.getData().toString();
                    list=gson.fromJson(Str, new TypeToken<List<PushInfo>>() {}.getType());
                    if ("0".equals(result.getCount())) {
                        message.obj = list;
                        message.what = SELECT_PUSHINFO_RESULT;
                        handler.sendMessage(message);
                    }else {
                        message.obj = list;
                        message.what = SELECT_PUSHINFO_RESULT;
                        handler.sendMessage(message);
                    }
                } catch (Exception e) {
                    Message messageError = new Message();
                    messageError.what = SELECT_PUSHINFO_RESULT_ERROR;
                    handler.sendMessage(messageError);
                    e.printStackTrace();
                }
            }

        }).start();
    }

    //加载发布列表
    private  void setFragmentView(List<PushInfo> selectList){

        List<PushInfo> list = selectList;
        PushInfoAdapter pushInfoAdapter = new PushInfoAdapter(context,list);
        pbFindAll.setVisibility(View.GONE);
        lvFindAll.setAdapter(pushInfoAdapter);
        lvFindAll.setOnItemClickListener(oick);
        pushInfoListLoad();

    }

    private void pushInfoListLoad() {
        //列表上拉下拉操作
        lvFindAll.setonRefreshListener(new ListLoadUtil.onRefreshListener() {
            //下拉操作
            @Override
            public void refresh() {
                new  Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        index=0;
                        pushinfolist.clear();
                        lvFindAll.setOnRefreshComplete();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                getPushInfoThread();
                            }
                        }).start();
                    }
                }.sendEmptyMessageDelayed(0,3000);
            }

            //上拉操作
            @Override
            public void loadingMore() {
                new  Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        lvFindAll.setOnRefreshComplete();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                getPushInfoThread();
                            }
                        }).start();
                    }
                }.sendEmptyMessageDelayed(0,3000);
            }
        });
    }



    //点击ListView
    AdapterView.OnItemClickListener oick=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PushInfo pushInfo=pushinfolist.get(position-1);
            openPushInfo(pushInfo.getPushInfoId());
        }
    };

    //页面跳转
    public void openPushInfo(long pushInfoId){
        Intent intent=new Intent(context, PropertyActivity.class);
        intent.putExtra("pushInfoId",pushInfoId);
        startActivityForResult(intent,SHOW_PUSHINFO);
    }





}
