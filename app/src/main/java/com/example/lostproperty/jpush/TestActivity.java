package com.example.lostproperty.jpush;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.example.lostproperty.R;

import cn.jpush.android.api.JPushInterface;

public class TestActivity extends Activity {
    private TextView tvContent,tvTitle;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_jpush_show);
        init();
    }

    private void init() {
        tvContent=findViewById(R.id.tvContent);
        tvTitle=findViewById(R.id.tvTitle);
        intent = getIntent();
        if (null != intent) {
            Bundle bundle = getIntent().getExtras();
            String title = null;
            String content = null;
            if(bundle!=null){
                title = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
                content = bundle.getString(JPushInterface.EXTRA_ALERT);
            }
            if(!"".equals(title)||title!=null||!"".equals(content)||content!=null) {
                tvTitle.setText(title);
                tvContent.setText(Html.fromHtml(content));
            }else{
                tvTitle.setText("");
                tvContent.setText("公告获取失败");
            }
        }
    }

}
