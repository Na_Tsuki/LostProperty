package com.example.lostproperty.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.jpush.TestActivity;
import com.example.lostproperty.model.NewPushInfo;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.ui.PropertyActivity;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.NotificationUtil;
import com.example.lostproperty.utils.SharedPreferencesUtils;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class NewPushInfoService extends Service {


    public NewPushInfoService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new NewPushInfoBinder();
    }

    public class NewPushInfoBinder extends Binder {
        private static final int RESULT_OK = 1;
        private static final int RESULT_ERROR = -9999;
        private static final int RESULT_ERROR_NULL = -9998;
        private boolean isGetNew = true;//是否循环查询

        private Gson gson = new Gson();
        Handler handler = new Handler() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case RESULT_OK:
                        NewPushInfo newPushInfo = (NewPushInfo) msg.obj;
                        NotificationUtil.sendSimpleNotify(NewPushInfoService.this, "新评论", "新评论", "66", "您发布的帖子" + newPushInfo.getPushInfo().getTitle() + "收到新评论了", getPendingIntentDemo(newPushInfo.getPushInfo().getPushInfoId()), R.drawable.ic_comment);
                        break;
                    case RESULT_ERROR:
                        break;
                }
                if (isGetNew) {
                    getNewPushInfo(1000 * 60 * 3);
                }
            }
        };

        public boolean isGetNew() {
            return isGetNew;
        }

        public void setGetNew(boolean getNew) {
            isGetNew = getNew;
        }

        public PendingIntent getPendingIntentDemo(long pushInfoId) {
            //页面跳转
            Intent intent = new Intent(NewPushInfoService.this, PropertyActivity.class);
            intent.putExtra("pushInfoId", pushInfoId);
            PendingIntent pendingIntent = PendingIntent.getActivity(NewPushInfoService.this, R.string.app_name, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            return pendingIntent;
        }

        public void getNewPushInfo(final long sleepTime) {
            Message message = new Message();
            String userId= SharedPreferencesUtils.getData(NewPushInfoService.this,"user","struser_id");
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(sleepTime);
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put("userId", userId);
                        String resultJson = HttpClientUtils.HttpClientGet(IpConfig.ROOTURL_Kittlen + "newPushInfo/selectByUser", map);
                        Result result = Result.toResult(resultJson);
                        if (result.getCode() == Result.RESULT_OK) {
                            NewPushInfo newPushInfo = gson.fromJson(result.getData().toString(), NewPushInfo.class);
                             message.obj = newPushInfo;
                            message.what = RESULT_OK;
                        } else {
                            message.what = RESULT_ERROR;
                        }
                    } catch (InterruptedException e) {
                        message.what = RESULT_ERROR;
                        e.printStackTrace();
                    } catch (Exception e) {
                        message.what = RESULT_ERROR_NULL;
                        e.printStackTrace();
                    }
                    handler.sendMessage(message);
                }
            });
            thread.start();
        }


    }
}
