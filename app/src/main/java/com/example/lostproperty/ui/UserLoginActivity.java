package com.example.lostproperty.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.*;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.NewPushInfo;
import com.example.lostproperty.model.PushInfo;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.model.User;
import com.example.lostproperty.service.NewPushInfoService;
import com.example.lostproperty.utils.Base64Utils;
import com.example.lostproperty.utils.Constant;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.LoadingDialog;
import com.example.lostproperty.utils.MD5Utils;
import com.example.lostproperty.utils.NotificationUtil;
import com.example.lostproperty.utils.SharedPreferencesUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class UserLoginActivity extends AppCompatActivity
        implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private final static int RESULT_EASEMOBLOGIN_SUCCESS = 1111;
    private static final int RESULT_EASEMOBLOGIN_ERROR = 1101;
    private static final int RESULT_EASEMOB_REGISTER_SUCCESS = 1112;
    private static final int RESULT_EASEMOB_REGISTER_ERROR = 1102;
    //布局内的控件
    private TextView tvBack;
    private TextView tvRegister;
    private EditText etPhone;
    private EditText etPassword;
    private Button mLoginBtn;
    private CheckBox cbPassword;
    private CheckBox cbLogin;
    private ImageView ivSeePassword;
    private Context context;
    private User loginUser = null;
    private LoadingDialog mLoadingDialog; //显示正在加载的对话框
    private NewPushInfoService.NewPushInfoBinder binder = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        setupEvents();
        initData();
        npiServiceInit();
    }

    /**
     * 加载视图
     */
    private void initViews() {
        tvBack = findViewById(R.id.loginback);
        tvRegister = findViewById(R.id.btnregister);
        mLoginBtn = findViewById(R.id.btnlogin);
        etPhone = findViewById(R.id.loginphone);
        etPassword = findViewById(R.id.loginpassword);
        cbPassword = findViewById(R.id.cbpassword);
        cbLogin = findViewById(R.id.cblogin);
        ivSeePassword = findViewById(R.id.ivseepassword);
    }

    /**
     * 加载事件
     */
    private void setupEvents() {
        mLoginBtn.setOnClickListener(this);
        cbPassword.setOnCheckedChangeListener(this);
        cbLogin.setOnCheckedChangeListener(this);
        ivSeePassword.setOnClickListener(this);
        tvBack.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    /**
     * 加载数据
     */
    private void initData() {


        //判断用户第一次登陆
        if (firstLogin()) {
            cbPassword.setChecked(false);//取消记住密码的复选框
            cbLogin.setChecked(false);//取消自动登录的复选框
        }
        //判断是否记住密码
        if (rememberPassword()) {
            cbPassword.setChecked(true);//勾选记住密码
            setTextNameAndPassword();//把密码和账号输入到输入框中
        } else {
            setTextName();//把用户账号放到输入账号的输入框中
        }

        //判断是否自动登录
        if (autoLogin()) {
            cbLogin.setChecked(true);
            login();//去登录就可以

        }
    }

    /**
     * 把本地保存的数据设置数据到输入框中
     */
    public void setTextNameAndPassword() {
        etPhone.setText("" + getLocalName());
        etPassword.setText("" + getLocalPassword());
    }

    /**
     * 设置数据到输入框中
     */
    public void setTextName() {
        etPhone.setText("" + getLocalName());
    }


    /**
     * 获得保存在本地的用户名
     */
    public String getLocalName() {
        //获取SharedPreferences对象，使用自定义类的方法来获取对象
        SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");
        String name = helper.getString("name");
        return name;
    }

    /**
     * 获得保存在本地的密码
     */
    public String getLocalPassword() {
        //获取SharedPreferences对象，使用自定义类的方法来获取对象
        SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");
        String password = helper.getString("password");
        return Base64Utils.decryptBASE64(password);   //解码一下

    }

    /**
     * 判断是否自动登录
     */
    private boolean autoLogin() {
        //获取SharedPreferences对象，使用自定义类的方法来获取对象
        SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");
        boolean autoLogin = helper.getBoolean("autoLogin", false);
        return autoLogin;
    }

    /**
     * 判断是否记住密码
     */
    private boolean rememberPassword() {
        //获取SharedPreferences对象，使用自定义类的方法来获取对象
        SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");
        boolean rememberPassword = helper.getBoolean("rememberPassword", false);
        return rememberPassword;
    }

    /**
     * 判断是否是第一次登陆
     */
    private boolean firstLogin() {
        //获取SharedPreferences对象，使用自定义类的方法来获取对象
        SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");
        boolean first = helper.getBoolean("first", true);
        if (first) {
            //创建一个ContentVa对象（自定义的）设置不是第一次登录，,并创建记住密码和自动登录是默认不选，创建账号和密码为空
            helper.putValues(new SharedPreferencesUtils.ContentValue("first", false),
                    new SharedPreferencesUtils.ContentValue("rememberPassword", false),
                    new SharedPreferencesUtils.ContentValue("autoLogin", false),
                    new SharedPreferencesUtils.ContentValue("name", ""),
                    new SharedPreferencesUtils.ContentValue("password", ""));
            return true;
        }
        return false;
    }

    /**
     * 点击事件
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnlogin:
                loadUserName();    //无论如何保存一下用户名
                login(); //登陆
                break;
            case R.id.ivseepassword:
                setPasswordVisibility();    //改变图片并设置输入框的文本可见或不可见
                break;
            case R.id.loginback:
                Intent intent = new Intent(this,MainActivity.class);
                intent.putExtra("id",1);
                startActivity(intent);
                finish();
                break;
            case R.id.btnregister:
                Intent registerIntent = new Intent(this, UserRegisterActivity.class);
                startActivity(registerIntent);
                finish();
                break;
        }
    }

    /**
     * 执行登录
     */
    private void login() {
        showLoading();//显示加载框
        setLoginBtnClickable(false);//点击登录后，设置登录按钮不可点击状态
        Thread loginRunnable = new Thread() {
            @Override
            public void run() {
                super.run();
                //睡眠1秒
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //判断账号和密码
                getLoginThread();
            }
        };
        loginRunnable.start();
    }


    /**
     * 保存用户账号
     */
    public void loadUserName() {
        if (!getAccount().equals("") || !getAccount().equals("请输入登录账号")) {
            SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");
            helper.putValues(new SharedPreferencesUtils.ContentValue("name", getAccount()));
        }

    }

    /**
     * 设置密码可见和不可见的相互转换
     */
    private void setPasswordVisibility() {
        if (ivSeePassword.isSelected()) {
            ivSeePassword.setSelected(false);
            //密码不可见
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            ivSeePassword.setSelected(true);
            //密码可见
            etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }

    }

    /**
     * 获取账号
     */
    public String getAccount() {
        return etPhone.getText().toString().trim();//去掉空格
    }

    /**
     * 获取密码
     */
    public String getPassword() {
        return etPassword.getText().toString().trim();//去掉空格
    }


    /**
     * 保存用户选择“记住密码”和“自动登陆”的状态
     */
    private void loadCheckBoxState() {
        loadCheckBoxState(cbPassword, cbLogin);
    }

    /**
     * 保存按钮的状态值
     */
    public void loadCheckBoxState(CheckBox checkBox_password, CheckBox checkBox_login) {

        //获取SharedPreferences对象，使用自定义类的方法来获取对象
        SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");

        //如果设置自动登录
        if (checkBox_login.isChecked()) {
            //创建记住密码和自动登录是都选择,保存密码数据
            helper.putValues(
                    new SharedPreferencesUtils.ContentValue("rememberPassword", true),
                    new SharedPreferencesUtils.ContentValue("autoLogin", true),
                    new SharedPreferencesUtils.ContentValue("password", Base64Utils.encryptBASE64(getPassword())));

        } else if (!checkBox_password.isChecked()) { //如果没有保存密码，那么自动登录也是不选的
            //创建记住密码和自动登录是默认不选,密码为空
            helper.putValues(
                    new SharedPreferencesUtils.ContentValue("rememberPassword", false),
                    new SharedPreferencesUtils.ContentValue("autoLogin", false),
                    new SharedPreferencesUtils.ContentValue("password", ""));
        } else if (checkBox_password.isChecked()) {   //如果保存密码，没有自动登录
            //创建记住密码为选中和自动登录是默认不选,保存密码数据
            helper.putValues(
                    new SharedPreferencesUtils.ContentValue("rememberPassword", true),
                    new SharedPreferencesUtils.ContentValue("autoLogin", false),
                    new SharedPreferencesUtils.ContentValue("password", Base64Utils.encryptBASE64(getPassword())));
        }
    }

    /**
     * 是否可以点击登录按钮
     */
    public void setLoginBtnClickable(boolean clickable) {
        mLoginBtn.setClickable(clickable);
    }

    /**
     * 显示加载的进度款
     */
    public void showLoading() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this, "正在加载数据", false);
        }
        mLoadingDialog.show();
    }


    /**
     * 隐藏加载的进度框
     */
    public void hideLoading() {
        if (mLoadingDialog != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLoadingDialog.hide();
                }
            });

        }
    }

    /**
     * 设置数据到输入框中
     */
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView == cbPassword) {  //记住密码选框发生改变时
            if (!isChecked) {   //如果取消“记住密码”，那么同样取消自动登陆
                cbLogin.setChecked(false);
            }
        } else if (buttonView == cbLogin) {   //自动登陆选框发生改变时
            if (isChecked) {   //如果选择“自动登录”，那么同样选中“记住密码”
                cbPassword.setChecked(true);
            }
        }
    }

    /**
     * 监听回退键
     */
    @Override
    public void onBackPressed() {
        if (mLoadingDialog != null) {
            if (mLoadingDialog.isShowing()) {
                mLoadingDialog.cancel();
            } else {
                finish();
            }
        } else {
            finish();
        }

    }


    /**
     * 页面销毁前回调的方法
     */
    protected void onDestroy() {
        if (mLoadingDialog != null) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
        super.onDestroy();
    }

    /**
     * 弹窗工具
     */
    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(UserLoginActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case RESULT_OK:
                    loginUser = (User) msg.obj;
                    String password = MD5Utils.getMD5Code(getPassword());
                    if (loginUser.getPassword().equals(password)) {
                        if (binder != null) {
                            binder.getNewPushInfo(0);
                        } else {
                            npiServiceInit();
                            if (binder != null) {
                                binder.getNewPushInfo(0);
                            }
                        }
                        easemobLogin();
                        break;
                    } else {
                        showToast("用户名或密码错误");
                        cbLogin.setChecked(false);
                        cbPassword.setChecked(false);
                        loadCheckBoxState();
                        break;
                    }
                case RESULT_CANCELED:
                    showToast("用户不存在");
                    break;
                case RESULT_EASEMOBLOGIN_SUCCESS:
                    showToast("登录成功");
                    loginSuccess(loginUser);
                    break;
                case RESULT_EASEMOBLOGIN_ERROR:
                    showToast("用户名或密码错误");
                    break;
                case RESULT_EASEMOB_REGISTER_SUCCESS:
                    easemobLogin();
                    break;

            }
        }
    };

    /**
     * 登录完成确认登录状态
     */
    private void isLoginOk() {
        SharedPreferences spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        String username = spUser.getString("username", "");
        if (username != null && !"".equals(username)) {
            hideLoading();//隐藏加载框
            loadCheckBoxState();
            Intent intent = new Intent(this,MainActivity.class);
            intent.putExtra("id",1);
            startActivity(intent);
            finish();
        }

    }

    /**
     * 登陆成功
     */
    private void loginSuccess(User user) {
        SharedPreferences sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("struser_id", user.getStrUserId());
        editor.putLong("user_id", user.getUserId());
        editor.putString("password", user.getPassword());
        editor.putString("username", user.getUserName());
        editor.putString("nickname", user.getNickName());
        editor.putString("avatar", user.getAvatar());
        editor.putString("phone", user.getPhone());
        editor.putInt("gender", user.getGender());
        editor.putString("createddate", user.getCreatedDate());
        editor.commit();
        isLoginOk();
    }

    /**
     * 登陆线程
     */
    private void getLoginThread() {

        new Thread(new Runnable() {
            @Override

            public void run() {
                Gson gson = new Gson();
                Message message = new Message();
                String userName = getAccount();
                String resultJson = null;
                Map<String, Object> map = new HashMap<>();
                map.put("userName", userName);
                try {
                    resultJson = HttpClientUtils.HttpClientPost(IpConfig.ROOTURL_YEN_LUY + "user/getUserByName", map);
                    Result result = gson.fromJson(resultJson, Result.class);

                    if (result.getCode() == 0) {
                        User user = gson.fromJson(result.getData().toString(), User.class);
                        message.obj = user;
                        message.what = RESULT_OK;

                    } else {
                        message.what = RESULT_CANCELED;
                    }

                    handler.handleMessage(message);

                } catch (Exception e) {
                    showToast("网络请求失败！");
                    e.printStackTrace();
                }
                setLoginBtnClickable(true);  //可按登录按钮
                hideLoading();//隐藏加载框
            }
        }).start();

    }


    //环信登陆
    private void easemobLogin() {
//        String nameStr = getAccount();
        String id = getAccount();
        String pwd = MD5Utils.UseMD5(getPassword());
        if (id == null || "".equals(id) || pwd == null || "".equals(pwd)) {
            Toast.makeText(this, "账号和密码不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        Message message = new Message();
        EMClient.getInstance().login(id, pwd, new EMCallBack() {
            @Override
            public void onSuccess() {
                Log.i("EMClient", "登陆成功");
                message.what = RESULT_EASEMOBLOGIN_SUCCESS;
                handler.handleMessage(message);
            }

            @Override
            public void onError(int code, String error) {
                Log.e("EMClient", "登陆失败:" + "\r" + error + "\n" + code);
                if (code == 200) {//已登录
                    message.what = RESULT_EASEMOBLOGIN_SUCCESS;
                    handler.handleMessage(message);
                    return;
                }
                if(code==204){
                    register(id,pwd);
                    return;
                }
                message.what = RESULT_EASEMOBLOGIN_ERROR;
                handler.handleMessage(message);
            }

            @Override
            public void onProgress(int progress, String status) {

            }
        });
    }

    private void register(String id,String password){
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {Message message=new Message();
                    EMClient.getInstance().createAccount(id,password);
                    message.what = RESULT_EASEMOB_REGISTER_SUCCESS;
                    handler.handleMessage(message);
                    Log.i("EMClient", "注册成功");
                } catch (HyphenateException e) {
                    Message message=new Message();
                    message.what = RESULT_EASEMOB_REGISTER_ERROR;
                    handler.handleMessage(message);
                    e.printStackTrace();
                    Log.e("EMClient", "注册失败"+"\r"+e );
                }
            }
        }).start();
    }

    private void npiServiceInit() {
        Intent service = new Intent(this, NewPushInfoService.class);
        UserLoginActivity.npiServiceConnection conn = new UserLoginActivity.npiServiceConnection();
        //在activity中要得到service对象进而能调用对象的方法，但同时又不希望activity finish的时候service也被destory了
        startService(service);
        //startService和bindService混合使用
        bindService(service, conn, BIND_AUTO_CREATE);
    }

    public class npiServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = (NewPushInfoService.NewPushInfoBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    }


}
