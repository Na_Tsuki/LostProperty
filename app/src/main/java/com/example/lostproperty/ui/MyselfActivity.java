package com.example.lostproperty.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.alibaba.fastjson.JSON;
import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.model.User;
import com.example.lostproperty.utils.HandleOSImagePath;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.ImageLoaderUtil;
import com.example.lostproperty.utils.LoadingDialog;
import com.example.lostproperty.utils.Permissions;
import com.example.lostproperty.utils.RoundImageView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyselfActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int RESULT_UPLOAD_OK = 1101;
    private static final int RESULT_UPLOAD_CANCELED = 1102;
    private static final int REQUEST_CODE_SYS_IMG = 1112;
    private ImageLoader loader = ImageLoader.getInstance();
    private LinearLayout llChange, llPhoto, llName, llGender, llPb, llSex;
    private View vChange;
    private TextView tvChange, tvId, tvName, tvGender, tvOK, tvBack;
    private EditText etChange;
    private Button btnBreak, btnOk;
    private RoundImageView ivPhoto;
    private RadioGroup rgSex;
    private RadioButton rbMan, rbWoman;
    private String fileName;
    private Map<String, String> rfileMap = new HashMap<>();
    private User user = new User();
    private String imagePath;
    private String Url = IpConfig.ROOTURL_YEN_LUY + "/upload/" + user.getUserName();
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private LoadingDialog mLoadingDialog; //显示正在加载的对话框
    private Context context;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        //请求文件读写权限
        Permissions.verifyStoragePermissions(this);
        //请求网络权限
        Permissions.verifyInternetPermissions(this);
        ImageLoaderUtil.ImageLoaderInit(this);//初始化配置
        init();
        initDate();
        setupEvents();
    }

    private void initDate() {
        SharedPreferences spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        user.setUserId(spUser.getLong("user_id", 0));
        user.setUserName(spUser.getString("username", ""));
        user.setGender(spUser.getInt("gender", 0));
        user.setPhone(spUser.getString("phone", ""));
        user.setNickName(spUser.getString("nickname", ""));
        user.setPassword(spUser.getString("password", ""));
        user.setAvatar(spUser.getString("avatar", ""));
        user.setCreatedDate(spUser.getString("createddate", ""));
        tvId.setText(user.getStrUserId());
        tvName.setText(user.getNickName());
        int gender = spUser.getInt("gender", 0);
        String strGender = "未知";
        if (gender == 1) {
            strGender = "男";
        } else if (gender == 2) {
            strGender = "女";
        }
        tvGender.setText(strGender);
        ImageLoaderUtil.ImageLoaderInit(this);
        if (user.getAvatar().equals("123")) {
            ivPhoto.setImageResource(R.drawable.appicon);
        } else {
            imageLoader.displayImage(IpConfig.ROOTURL_IMAGE + user.getUserName() + "/" + user.getAvatar(), ivPhoto);
        }
    }

    private void setupEvents() {
        llPhoto.setOnClickListener(this);
        llName.setOnClickListener(this);
        llGender.setOnClickListener(this);
        vChange.setOnClickListener(this);
        btnBreak.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        tvOK.setOnClickListener(this);
        tvBack.setOnClickListener(this);
    }

    private void init() {
        llChange = findViewById(R.id.llChange);
        llPb = findViewById(R.id.llPb);
        llPhoto = findViewById(R.id.llPhoto);
        llName = findViewById(R.id.llName);
        llGender = findViewById(R.id.llGender);
        vChange = findViewById(R.id.vChange);
        tvChange = findViewById(R.id.tvChange);
        tvId = findViewById(R.id.tvId);
        tvName = findViewById(R.id.tvName);
        tvGender = findViewById(R.id.tvGender);
        etChange = findViewById(R.id.etChange);
        btnBreak = findViewById(R.id.btnBreak);
        btnOk = findViewById(R.id.btnOk);
        ivPhoto = findViewById(R.id.ivPhoto);
        rgSex = findViewById(R.id.rgSex);
        rbMan = findViewById(R.id.rbMan);
        rbWoman = findViewById(R.id.rbWoman);
        llSex = findViewById(R.id.llSex);
        tvOK = findViewById(R.id.tv_m_OK);
        tvBack = findViewById(R.id.tv_m_back);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llPhoto:
                openImage();
                break;
            case R.id.llName:
                change(1);
                break;
            case R.id.llGender:
                change(2);
                break;
            case R.id.vChange:
                change(0);
                break;
            case R.id.btnBreak:
                change(0);
                break;
            case R.id.btnOk:
                changeOk();
                change(0);
                break;
            case R.id.tv_m_back:
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("id", 1);
                startActivity(intent);
                finish();
                break;
            case R.id.tv_m_OK:
                upload();
                break;
        }
    }

    private void upload() {
        showLoading();//显示加载框
        Thread uploadRunnable = new Thread() {
            @Override
            public void run() {
                super.run();
                //睡眠1秒
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishNewImg();
                publishUser();
            }
        };
        uploadRunnable.start();
    }

    //打开相册
    private void openImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_CODE_SYS_IMG);
    }

    private void change(int index) {
        switch (index) {
            case 0:
                llChange.setVisibility(View.GONE);
                initDate();
                break;
            case 1:
                llChange.setVisibility(View.VISIBLE);
                etChange.setVisibility(View.VISIBLE);
                llSex.setVisibility(View.GONE);
                tvChange.setText("昵称");
                etChange.setText(user.getNickName());
                break;
            case 2:
                llChange.setVisibility(View.VISIBLE);
                etChange.setVisibility(View.GONE);
                llSex.setVisibility(View.VISIBLE);
                tvChange.setText("性别");
                switch (user.getGender()) {
                    case 0:
                        break;
                    case 1:
                        rbMan.setChecked(true);
                        break;
                    case 2:
                        rbWoman.setChecked(true);
                        break;
                }

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case REQUEST_CODE_SYS_IMG:
                if (resultCode == RESULT_OK) {
                    String imagePath = HandleOSImagePath.ImagePath(this, intent);
                    if (imagePath != null) {
                        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
                        ivPhoto.setImageBitmap(bitmap);
                        String filefix = imagePath.substring(imagePath.lastIndexOf("."));
                        //根据UserName生成上传文件名
                        fileName = user.getUserName() + filefix;
                        user.setAvatar(fileName);
                        //加入文件Map
                        rfileMap.put(fileName, imagePath);
                    } else {
                        showToast("图片获取失败");
                    }
                    break;
                }
                break;

        }
    }

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyselfActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void changeOk() {
        SharedPreferences spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        switch (tvChange.getText().toString()) {
            case "昵称":
                String spNickName = spUser.getString("nickname", "");
                String lastStr = etChange.getText().toString().trim();
                if (spNickName.equals(lastStr)) {
                    showToast("未有修改");
                } else {
                    SharedPreferences.Editor editor = spUser.edit();
                    editor.putString("nickname", lastStr);
                    editor.commit();
                    showToast(lastStr);
                    llChange.setVisibility(View.GONE);
                }

                break;
            case "性别":
                int spGender = spUser.getInt("gender", 0);
                int gender = 0;
                if (rbMan.isChecked()) {
                    gender = 1;
                } else {
                    gender = 2;
                }
                if (spGender != 0 && spGender == gender) {
                    showToast("未有修改");
                } else {
                    SharedPreferences.Editor editor = spUser.edit();
                    editor.putInt("gender", gender);
                    editor.commit();
                    llChange.setVisibility(View.GONE);
                }

                break;

        }
    }


    private Map<String, File> findUploadImg() {
        Map<String, File> fileMap = new HashMap<>();
        if (rfileMap != null && fileName != null) {
            String path = rfileMap.get(fileName);
            fileMap.put(fileName, new File(path));
        }
        return fileMap;
    }

    //上传图片
    private void publishNewImg() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                Map<String, File> fileMap = findUploadImg();
                try {
                    Map<String, String> umap = new HashMap<>();
                    umap.put("dic", user.getPhone());
                    //上传图片
                    HttpClientUtils.HttpMultipartPost(IpConfig.ROOTURL_YEN_LUY + "user/multipartAction", umap, fileMap);
                    showToast("图片上传成功");
                } catch (Exception e) {
                    e.printStackTrace();
                    showToast("图片上传失败");
                }
            }
        }).start();
    }

    //修改用户信息
    private void publishUser() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                Message message = new Message();
                String resultJson = null;
                Map<String, Object> map = new HashMap<>();
                final String string = JSON.toJSONString(user);
                map.put("upuser", string);
                try {
                    resultJson = HttpClientUtils.HttpClientPost(IpConfig.ROOTURL_YEN_LUY + "user/updateUser", map);
                    Result result = gson.fromJson(resultJson, Result.class);
                    if (result.getCode() == 0) {
                        message.what = RESULT_OK;

                    } else {
                        message.what = RESULT_CANCELED;
                    }
                    handler.handleMessage(message);
                } catch (Exception e) {
                    showToast("网络请求失败！");
                    e.printStackTrace();
                }
                hideLoading();
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case RESULT_OK:
                    showToast("信息修改成功,请重新登录");
                    reLogin();
                    break;
                case RESULT_CANCELED:
                    showToast("信息修改出错请重新尝试");
                    break;
                case RESULT_UPLOAD_OK:
                    showToast("头像上传成功,请重新登录");
                    break;
                case RESULT_UPLOAD_CANCELED:
                    showToast("头像上传出错请重新尝试");
                    break;
            }
        }
    };

    private void reLogin() {
        SharedPreferences spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        spUser.edit().clear().commit();
        Intent intent = new Intent(this, UserLoginActivity.class);
        startActivity(intent);
        finish();
    }


    /**
     * 显示加载的进度款
     */
    public void showLoading() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this, "正在加载数据", false);
        }
        mLoadingDialog.show();
    }


    /**
     * 隐藏加载的进度框
     */
    public void hideLoading() {
        if (mLoadingDialog != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLoadingDialog.hide();
                }
            });

        }
    }

    /**
     * 监听回退键
     */
    @Override
    public void onBackPressed() {
        if (mLoadingDialog != null) {
            if (mLoadingDialog.isShowing()) {
                mLoadingDialog.cancel();
            } else {
                finish();
            }
        } else {
            finish();
        }

    }


    /**
     * 页面销毁前回调的方法
     */
    protected void onDestroy() {
        if (mLoadingDialog != null) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
        super.onDestroy();
    }


}

