package com.example.lostproperty.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lostproperty.R;
import com.example.lostproperty.mainfragment.MainmenuFragment;
import com.example.lostproperty.mainfragment.MessageFragment;
import com.example.lostproperty.mainfragment.MyselfFragment;
import com.example.lostproperty.service.NewPushInfoService;
import com.example.lostproperty.utils.Constant;
import com.example.lostproperty.utils.Permissions;
import com.example.lostproperty.utils.SharedPreferencesUtils;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.easeui.EaseConstant;
import com.hyphenate.easeui.ui.EaseConversationListFragment;

import java.util.HashSet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static boolean isForeground = false;
    public static final String MESSAGE_RECEIVED_ACTION = "com.example.jpushdemo.MESSAGE_RECEIVED_ACTION";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";

    private MainmenuFragment mainmenuFragment;
    private MessageFragment messageFragment;
    private MyselfFragment myselfFragment;

    private LinearLayout llt1;
    private LinearLayout llt2;
    private LinearLayout llt3;

    private ImageView img1;
    private ImageView img2;
    private ImageView img3;

    private TextView tv1;
    private TextView tv2;
    private TextView tv3;

    private ImageView insertmenu;
    private ImageView findSearch;
    private NewPushInfoService.NewPushInfoBinder binder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Permissions.verifyStoragePermissions(this);
        Permissions.verifyInternetPermissions(this);
        init();
        npiServiceInit();
        mainmenuFragment = new MainmenuFragment();
        messageFragment = new MessageFragment();
        myselfFragment = new MyselfFragment();
        seltFragment();
    }

    private void init() {
        llt1 = findViewById(R.id.llt1);
        llt2 = findViewById(R.id.llt2);
        llt3 = findViewById(R.id.llt3);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        insertmenu = findViewById(R.id.insertmenu);
        findSearch = findViewById(R.id.findSearch);
        llt1.setOnClickListener(this);
        llt2.setOnClickListener(this);
        llt3.setOnClickListener(this);
        insertmenu.setOnClickListener(this);
        findSearch.setOnClickListener(this);
    }

    private void npiServiceInit() {
        String userId = SharedPreferencesUtils.getData(this, "user", "struser_id");
        if ("".equals(userId)) {
            return;
        }
        Intent service = new Intent(this, NewPushInfoService.class);
        npiServiceConnection conn = new npiServiceConnection();
        //在activity中要得到service对象进而能调用对象的方法，但同时又不希望activity finish的时候service也被destory了
        startService(service);
        //startService和bindService混合使用
        bindService(service, conn, BIND_AUTO_CREATE);
    }

    @SuppressLint("ResourceAsColor")
    private void seltFragment() {
        int id = getIntent().getIntExtra("id", 0);
        if (id == 1) {
            reLoadFragView();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.flt, mainmenuFragment).commit();
            img1.setImageResource(R.drawable.mainmenu_press);

        }
    }

    public class npiServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = (NewPushInfoService.NewPushInfoBinder) service;
            binder.getNewPushInfo(0);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    }

    //当在mainactivity按下返回键时，默认不退出进程，保持后台运行
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    @SuppressLint("ResourceAsColor")
    public void onClick(View view) {
        setViewColor();
        //点击时显示相应fragment
        switch (view.getId()) {
            case R.id.llt1:
                getSupportFragmentManager().beginTransaction().replace(R.id.flt, mainmenuFragment).commit();
                img1.setImageResource(R.drawable.mainmenu_press);
                break;
            case R.id.llt2:
                getSupportFragmentManager().beginTransaction().replace(R.id.flt, messageFragment).commit();
                img2.setImageResource(R.drawable.message_press);
                break;
            case R.id.llt3:
                getSupportFragmentManager().beginTransaction().replace(R.id.flt, myselfFragment).commit();
                img3.setImageResource(R.drawable.myself_press);
                break;
            case R.id.insertmenu:
                SharedPreferences sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
                String userName = sharedPreferences.getString("username", "");
                String pwd = sharedPreferences.getString("password", "");
                if (userName.equals("") || pwd.equals("")) {
                    Toast.makeText(this, "登录后就可以发帖啦", Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    Intent intent = new Intent(this, PublishActivity.class);
                    startActivityForResult(intent, 101);
                    break;
                }

            case R.id.findSearch:
                Intent intentSearch = new Intent(this, FindSearchActivity.class);
                startActivityForResult(intentSearch, Constant.FINDSEARCH_MESSAGE);
                break;
        }

    }


    private void setViewColor() {
        img1.setImageResource(R.drawable.mainmenu);
        img2.setImageResource(R.drawable.message);
        img3.setImageResource(R.drawable.myself);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == 102) {
                //从发布页面点击发布按钮后，返回此页面，并刷新
                this.getSupportFragmentManager().beginTransaction().replace(R.id.flt, new MainmenuFragment()).commit();
            }
            if (resultCode == 103) {
                //发布页点击返回后，返回此页面并刷新
                this.getSupportFragmentManager().beginTransaction().replace(R.id.flt, new MainmenuFragment()).commit();
            }

        }
        if (requestCode == Constant.FINDSEARCH_MESSAGE) {
            if (resultCode == Constant.FINDSEARCH_RESULT_MESSAGE) {

            }
        }
    }
    public void reLoadFragView() {
        this.getSupportFragmentManager().beginTransaction().replace(R.id.flt, new MyselfFragment()).commit();
    }

    }
