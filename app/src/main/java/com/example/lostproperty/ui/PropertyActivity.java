package com.example.lostproperty.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.adapter.CommentsAdapter;
import com.example.lostproperty.adapter.LikeAdapter;
import com.example.lostproperty.model.Comments;
import com.example.lostproperty.model.LikeMap;
import com.example.lostproperty.model.PushInfo;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.model.User;
import com.example.lostproperty.utils.BitmapCompress;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.ImageLoaderUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.easeui.EaseConstant;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertyActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int ERRORMSG = 4;
    private static final int LIKE_ERRORMSG = 5;
    private String url = IpConfig.ROOTURL_tuanzili;
    private static final int PUSH_INFO_OK = 1;
    private static final int COMMENT_OK = 2;
    private static final int LIKE_OK = 3;
    private static final int INSERT_LIKE_OK = 0;
    private List<Comments> commentsList = null;
    private List<LikeMap> likeMapList = null;

    private TextView tvTitle;
    private EditText tvContent;
    private ImageView ivHeadUrl;
    private TextView tvName;
    private TextView tvCreatedDate;
    private ImageView imgBack;
    private Map<String, Object> idMap;
    private TextView tvLike;
    private ImageView lvRelation;
    private TextView tvComment;
    private ListView lvContent;
    private Editable editable;
    private Handler handler;
    private ImageView ivComment;
    private ImageView ivLike;
    private String pushInfoId = "1";
    private String userId = "1";
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private User pushInfoUser=null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_porperty);
        ImageLoaderUtil.ImageLoaderInit(this);
        SharedPreferences sharedPreferences = getSharedPreferences("user", MODE_PRIVATE);
        long userId = sharedPreferences.getLong("user_id", 1);
        pushInfoId = String.valueOf(userId);
        Intent intent = getIntent();
        long id = intent.getExtras().getLong("pushInfoId", 1);
        pushInfoId = String.valueOf(id);
        initView();
        hideInput();
        getPushInfoById();

    }

    private void initView() {

        tvTitle = findViewById(R.id.textView17);
        tvContent = findViewById(R.id.textView24);
        tvContent.setText("");
        editable = tvContent.getText();
        ivHeadUrl = findViewById(R.id.imageView10);
        tvName = findViewById(R.id.textView10);
        tvCreatedDate = findViewById(R.id.textView11);
        lvContent = findViewById(R.id.lvContent);
        tvComment = findViewById(R.id.textView36);
        tvLike = findViewById(R.id.textView37);
        imgBack = findViewById(R.id.ivBack);
        ivComment = findViewById(R.id.imageView4);
        ivLike = findViewById(R.id.imageView5);
        lvRelation = findViewById(R.id.imageView21);

        imgBack.setOnClickListener(this);
        tvComment.setOnClickListener(this);
        tvLike.setOnClickListener(this);
        ivComment.setOnClickListener(this);
        ivLike.setOnClickListener(this);
        lvRelation.setOnClickListener(this);
        //初始化配置
        ImageLoaderUtil.ImageLoaderInit(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                Intent intent = new Intent();
                setResult(2000, intent);
                finish();
                break;
            case R.id.textView36:
                insertComment();
                break;
            case R.id.textView37:
                insertLike();
                break;
            case R.id.imageView4:
                pushComment();
                break;
            case R.id.imageView5:
                pointLike();
                break;
            case R.id.imageView21:
                relation();
                break;
        }

    }

    private void relation() {
        if(pushInfoUser==null){
            Toast.makeText(this, "获取对方用户信息失败", Toast.LENGTH_SHORT).show();
            return;
        }
        String chatId=pushInfoUser.getUserName();//此处填写此帖子的发布者id
        if (chatId == null || "".equals(chatId)) {
            Toast.makeText(this, "对方号码不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(EaseConstant.EXTRA_USER_ID, chatId);
        intent.putExtra(EaseConstant.EXTRA_CHAT_TYPE, EMMessage.ChatType.Chat);
        startActivity(intent);
    }

    private void pointLike() {
        ivLike.setImageResource(R.drawable.ic_givelike);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Message likeMapMessage = new Message();
                    idMap = null;
                    idMap = new HashMap<>();
                    String createdDate = getNowDate();
                    idMap.put("createdDate", createdDate);
                    System.out.println(createdDate);
                    idMap.put("userId", userId);
                    idMap.put("pushInfoId", pushInfoId);
                    String result = HttpClientUtils.HttpClientPost(url + "likeMap/insert", idMap);
                    likeMapMessage.what = INSERT_LIKE_OK;
                    likeMapMessage.obj = result;
                    handler.sendMessage(likeMapMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                    Message errMessage = new Message();
                    errMessage.what = LIKE_ERRORMSG;
                    handler.sendMessage(errMessage);
                }

            }
        }).start();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == INSERT_LIKE_OK) {
                    LikeMapOk();
                }
                if (msg.what == LIKE_ERRORMSG) {
                    likeResultError();
                }
            }
        };
    }

    private void LikeMapOk() {
        Toast.makeText(this, "点赞成功", Toast.LENGTH_LONG).show();
        getPushInfoById();
    }

    private void likeResultError() {
        Toast.makeText(this, "网络请求失败", Toast.LENGTH_LONG).show();
    }

    private void pushComment() {
        // 显示评论框
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_comment, null);
        CommentDialog dialogView = new CommentDialog(this, view, true, true, pushInfoId, userId);
        dialogView.show();

        getPushInfoById();
    }

    // 从服务器根据id获取发布信息数据
    private void getPushInfoById() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    // 获取发布信息
                    Message pushInfoMsg = new Message();
                    idMap = new HashMap<>();
                    idMap.put("id", pushInfoId);
                    String pushResult = HttpClientUtils.HttpClientPost(url + "pushInfo/selectById", idMap);
                    pushInfoMsg.what = PUSH_INFO_OK;
                    pushInfoMsg.obj = pushResult;
                    handler.sendMessage(pushInfoMsg);


                    // 获取评论
                    Message commentMsg = new Message();
                    idMap.put("pushInfoId", pushInfoId);
                    String commentResult = HttpClientUtils.HttpClientPost(url + "comments/selectByPushInfoId", idMap);
                    commentMsg.what = COMMENT_OK;
                    commentMsg.obj = commentResult;
                    handler.sendMessage(commentMsg);

                    // 获取点赞
                    Message likeMsg = new Message();
                    String likeResult = HttpClientUtils.HttpClientPost(url + "likeMap/selectByPushInfoId", idMap);
                    likeMsg.what = LIKE_OK;
                    likeMsg.obj = likeResult;
                    handler.sendMessage(likeMsg);

                } catch (Exception e) {
                    e.printStackTrace();
                    Message errMessage = new Message();
                    errMessage.what = ERRORMSG;
                    handler.sendMessage(errMessage);
                }
            }
        }

        ).start();
        handler = new Handler() {

            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                if (msg.what == PUSH_INFO_OK) {
                    String message = msg.obj.toString();
                    resultPushInfo(message);
                } else if (msg.what == COMMENT_OK) {
                    String message = msg.obj.toString();
                    resultComment(message);
                } else if (msg.what == LIKE_OK) {
                    String message = msg.obj.toString();
                    resultLike(message);
                } else if (msg.what == ERRORMSG) {
                    String message = msg.obj.toString();
                    resultError(message);
                }

            }


        };
    }

    private void resultError(String message) {
        Toast.makeText(this, "网络请求失败", Toast.LENGTH_LONG).show();

    }

    private void resultLike(String message) {
        Gson gson = new Gson();
        Result result = gson.fromJson(message, Result.class);
        String date = result.getData().toString();
        Long count = result.getCount();
        tvLike.setText("点赞   " + count.toString());
        likeMapList = gson.fromJson(date, new TypeToken<List<LikeMap>>() {
        }.getType());
    }

    private void resultComment(String message) {
        Gson gson = new Gson();
        Result result = gson.fromJson(message, Result.class);
        String date = result.getData().toString();
        Long count = result.getCount();
        tvComment.setText("评论   " + count.toString());
        commentsList = gson.fromJson(date, new TypeToken<List<Comments>>() {
        }.getType());
    }


    private void insertComment() {
        if (commentsList != null) {
            CommentsAdapter commentsAdapter = new CommentsAdapter(this, commentsList);
            lvContent.setAdapter(commentsAdapter);
            setListViewHeightBasedOnChildren(lvContent);
        } else {
            Toast.makeText(this, "获取的数据为空", Toast.LENGTH_LONG).show();
        }
    }

    private void insertLike() {
        if (likeMapList != null) {
            LikeAdapter likeAdapter = new LikeAdapter(this, likeMapList);
            lvContent.setAdapter(likeAdapter);
            setListViewHeightBasedOnChildren(lvContent);
        } else {
            Toast.makeText(this, "获取的数据为空", Toast.LENGTH_LONG).show();
        }
    }

    // 从服务器获取的数据渲染
    private void resultPushInfo(String pushInfoMsg) {
        Gson gson = new Gson();
        Result result = gson.fromJson(pushInfoMsg, Result.class);
        String date = result.getData().toString();
        PushInfo pushInfo = gson.fromJson(date, PushInfo.class);
        pushInfoUser=pushInfo.getUser();
        tvTitle.setText(pushInfo.getTitle());
        tvContent.setText(pushInfo.getContent());
        tvName.setText(pushInfo.getUser().getUserName());
        tvCreatedDate.setText(pushInfo.getCreatedDate());
        ivHeadUrl.setImageResource(R.drawable.ic_circle);
        imageLoader.displayImage(pushInfo.getUser().getAvatar(), ivHeadUrl);

        String phone = pushInfo.getUser().getUserName();
        // 内容插入图片
        loadImage(pushInfo.getContent(), phone);

//        tvContent.setText(Html.fromHtml(pushInfo.getContent()));
    }


    private void loadImage(String content, String phone) {
        List<String> androidImg = findUploadImg(content);
        List<String> WebImg = findUploadWebImg(content);
        if (androidImg.size() > 0) {
            String imgTag = "<img src=([\\u4E00-\\u9FA5A-Za-z0-9%_/.]*)/>";
            Pattern pattern = Pattern.compile(imgTag);
            Matcher matcher = pattern.matcher(content);
            while (matcher.find()) {
                final String tag = matcher.group();//获取匹配正则表达式的文字
                String imgName = matcher.group(1);//获取匹配正则表达式内第一个括号内的文字
                final int start = content.indexOf(tag);//获取对应图片所在原字段的开始位置
                final int end = start + tag.length();//获取对应图片所在原字段的结束位置
                String url = IpConfig.ROOTURL_tuanzili + "upload/" + phone + "/" + imgName;
                imageLoader.loadImage(url, ImageLoaderUtil.getOptions(), new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        replaceImage(loadedImage, start, end, tag);
                    }
                });
            }
        } else if (WebImg.size() > 0) {
            Pattern p_image;
            Matcher m_image;
            String img = "";
//        String regEx_img = ""; //图片链接地址
            String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
            p_image = Pattern.compile
                    (regEx_img, Pattern.CASE_INSENSITIVE);
            m_image = p_image.matcher(content);
            while (m_image.find()) {
                // 得到<img />数据
                img = m_image.group();
                // 匹配<img>中的src数据
                Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
                while (m.find()) {
                    String finalImg = m.group();
                    String imgName = m.group(1);//获取匹配正则表达式内第一个括号内的文字
                    final int start = content.indexOf(img);//获取对应图片所在原字段的开始位置
                    final int end = start + img.length();//获取对应图片所在原字段的结束位置
                    imageLoader.loadImage(imgName, ImageLoaderUtil.getOptions(), new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            super.onLoadingComplete(imageUri, view, loadedImage);
                            replaceImage(loadedImage, start, end, finalImg);
                        }
                    });
                }
            }


        } else {
            tvContent.setText(content);
        }


    }


    // 内容插入图片
    private void replaceImage(Bitmap loadedImage, int start, int end, String imgTag) {
        try {
            int width = loadedImage.getWidth();
            Bitmap newBitmap = BitmapCompress.zoomImage(loadedImage, width > tvContent.getWidth() ? tvContent.getWidth() : 0);
            ImageSpan span = new ImageSpan(this, newBitmap);
            SpannableString spannableString = new SpannableString(imgTag);
            spannableString.setSpan(span, 0, imgTag.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            editable = tvContent.getText();
            editable.replace(start, end, spannableString);
            tvContent.setText(editable);
        } catch (Exception e) {
            tvContent.setText("错误");
        }
        hideInput();
    }

    //  隐藏键盘
    protected void hideInput() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View v = getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        if (listView == null) return;
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private String getNowDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }

    private List<String> findUploadImg(String content) {
        List<String> fileList = new ArrayList<>();
        String imgTag = "<img src=([\\u4E00-\\u9FA5A-Za-z0-9%_/.]*)/>";
        Pattern pattern = Pattern.compile(imgTag);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            String imgName = matcher.group(1);
            fileList.add(imgName);
        }

        return fileList;
    }

    private List<String> findUploadWebImg(String content) {
        List<String> webImg = new ArrayList<String>();
        String img = "";
        Pattern p_image;
        Matcher m_image;
//        String regEx_img = ""; //图片链接地址
        String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
        p_image = Pattern.compile
                (regEx_img, Pattern.CASE_INSENSITIVE);
        m_image = p_image.matcher(content);
        while (m_image.find()) {
            // 得到<img />数据
            img = m_image.group();
            // 匹配<img>中的src数据
            Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
            while (m.find()) {
                String Img = m.group(1);
                webImg.add(Img);
            }
        }
        return webImg;
    }

}
