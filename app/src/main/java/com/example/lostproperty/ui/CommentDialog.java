package com.example.lostproperty.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.utils.HttpClientUtils;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.widget.Toast.LENGTH_LONG;
import static com.example.lostproperty.IpConfig.ROOTURL_tuanzili;

public class CommentDialog extends Dialog {

    private static final int INSERT_COMMENT_OK = 1;
    private static final int ERRORMSG = 2;
    private boolean iscancelable;//控制点击dialog外部是否dismiss
    //    private boolean isBackCancelable;//控制返回键是否dismiss
    private boolean isBackCanCelable;//
    private View view;
    private Context context;
    private Handler handler;
    private EditText etContent;
    private TextView tvSend;
    private String url = ROOTURL_tuanzili;
    private String pushInfoId = "1";
    private String userId = "1";

    public CommentDialog(Context context, View view, boolean isCancelable, boolean isBackCancelable, String pushInfoId, String userId) {
        super(context, R.style.comment_style);
        this.context = context;
        this.view = view;
        this.iscancelable = isCancelable;
        this.isBackCanCelable = isBackCancelable;
        this.pushInfoId = pushInfoId;
        this.userId = userId;
        etContent = view.findViewById(R.id.editTextTextMultiLine3);
        tvSend = view.findViewById(R.id.tvSend);

        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String text = etContent.getText().toString().trim();
                System.out.println(text);
                if (text.length() == 0) {
                    Toast.makeText(getContext(), "输入的内容不能为空", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                Message commentMsg = new Message();
                                Map<String, Object> commentMap = new HashMap<>();
                                String createdDate = getNowDate();
                                System.out.println(createdDate + text);
                                commentMap.put("createdDate", createdDate);
                                commentMap.put("userId", userId);
                                commentMap.put("pushInfoId", pushInfoId);
                                commentMap.put("content", text);
                                String result = HttpClientUtils.HttpClientPost(url + "comments/insert", commentMap);
                                commentMsg.what = INSERT_COMMENT_OK;
                                commentMsg.obj = result;
                                handler.sendMessage(commentMsg);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Message errorMsg = new Message();
                                errorMsg.what = ERRORMSG;
                                handler.sendMessage(errorMsg);
                            }

                        }
                    }).start();
                    handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            if (msg.what == INSERT_COMMENT_OK) {
                                String commentMsg = msg.obj.toString();
                                Gson gson = new Gson();
                                Result result = gson.fromJson(commentMsg, Result.class);
                                String message = result.getMsg();
                                Toast.makeText(getContext(), message, LENGTH_LONG).show();
                            }
                            if (msg.what == ERRORMSG) {
                                String errorMsg = msg.obj.toString();
                                errorResult(errorMsg);
                            }
                        }
                    };
                }

            }
        });
    }

    private void errorResult(String commentMsg) {
        Toast.makeText(context, " 请求数据失败", LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(view);//这行一定要写在前面
        setCancelable(iscancelable);//点击外部不可dismiss
        setCanceledOnTouchOutside(isBackCanCelable);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
    }

    private String getNowDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }
}