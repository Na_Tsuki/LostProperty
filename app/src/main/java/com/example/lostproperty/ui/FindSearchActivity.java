package com.example.lostproperty.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.adapter.PushInfoAdapter;
import com.example.lostproperty.model.PushInfo;
import com.example.lostproperty.utils.Constant;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.ListLoadUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindSearchActivity extends AppCompatActivity implements View.OnClickListener {

    private SearchView searchView;
    private TextView scBack;
    private ProgressBar scPB;
    private ListLoadUtil findSearchList;

    private long pushInfoId = 0;
    public static int start = 0;
    public static boolean isRefresh = false;

    List<PushInfo> list = null;
    List<PushInfo> sumList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_search);
        
        initView();
        setListener();
        findSearchListOnClick();
    }

    private void initView() {
        searchView = findViewById(R.id.searchView);
        scBack = findViewById(R.id.scBack);
        scPB = findViewById(R.id.scPB);
        findSearchList = findViewById(R.id.findSearchList);

        searchView.onActionViewExpanded();
        setUnderLinetransparent(searchView);
        scBack.setOnClickListener(this);
    }

    private void setListener(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                scPB.setVisibility(View.VISIBLE);
                String title = searchView.getQuery().toString();
                final Map<String, Object> map = new HashMap<>();
                map.put("title", title);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String url = IpConfig.duzq +"pushInfo/getByTitle/"+start;
                        Message message = new Message();
                        try {
                            String str = HttpClientUtils.HttpClientGet(url,map);
                            JSONObject jsonObject = JSONObject.parseObject(str);
                            str = jsonObject.get("data").toString();
                            message.what = Constant.FINDSEARCH_MESSAGE_SUCCESS;
                            message.obj = str;
                            handler.sendMessage(message);
                        } catch (Exception e) {
                            message.what = Constant.GET_REQUEST_FAIL;
                            handler.sendMessage(message);
                            e.printStackTrace();
                        }
                    }

                }).start();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    public void findSearchListOnClick(){
        //详情操作
        findSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pushInfoId=sumList.get(position-1).getPushInfoId();
                Intent intent = new Intent(FindSearchActivity.this, PropertyActivity.class);
                intent.putExtra("pushInfoId",pushInfoId);
                startActivityForResult(intent,Constant.PUSHINFO_CONTENT_MESSAGE_SUCCESS);
            }
        });
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            scPB.setVisibility(View.GONE);
            switch (msg.what){
                //搜索列表
                case Constant.FINDSEARCH_MESSAGE_SUCCESS:
                    String str = msg.obj.toString();
                    if (str.equals("[]")){
                        findSearchList.setAdapter(null);
                        Toast.makeText(FindSearchActivity.this, "没有找到呀!", Toast.LENGTH_SHORT).show();
                    }else if(str.equals("noMore")){
                        Toast.makeText(FindSearchActivity.this, "暂无更多啦!", Toast.LENGTH_SHORT).show();
                    }else{
                        list = JSONObject.parseArray(str, PushInfo.class);
                        if (isRefresh){
                            sumList.clear();
                        }
                        sumList.addAll(list);
                        final PushInfoAdapter pushInfoAdapter = new PushInfoAdapter(FindSearchActivity.this,sumList);
                        if (start != 0){
                            findSearchList.invalidateViews();
                            pushInfoAdapter.notifyDataSetChanged();
                        }else{
                            findSearchList.setAdapter(pushInfoAdapter);
                        }
                    }
                    break;
                //请求出错
                case Constant.GET_REQUEST_FAIL:
                    break;
            }
        }
    };

    /**设置SearchView下划线透明**/
    private void setUnderLinetransparent(SearchView searchView){
        try {
            Class<?> argClass = searchView.getClass();
            // mSearchPlate是SearchView父布局的名字
            Field ownField = argClass.getDeclaredField("mSearchPlate");
            ownField.setAccessible(true);
            View mView = (View) ownField.get(searchView);
            mView.setBackgroundColor(Color.WHITE);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.scBack:
                back();
                break;
        }
    }

    private void back() {
        Intent intent = new Intent();
        setResult(Constant.FINDSEARCH_RESULT_MESSAGE,intent);
        finish();
    }
}
