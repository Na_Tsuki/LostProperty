package com.example.lostproperty.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.baidumap.BaiduMapActivity;
import com.example.lostproperty.model.PushInfo;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.model.Type;
import com.example.lostproperty.model.User;
import com.example.lostproperty.utils.DialogUtils;
import com.example.lostproperty.utils.HandleOSImagePath;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.Permissions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PublishActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_SYS_IMG = 10;
    private Map<String,String> rfileMap=new HashMap<>();
    private Map<String,File> fileMap=new HashMap<>();
    private String rootUrl = IpConfig.ROOTURL_Na_Tuski;
    private List<Type> list;
    private Editable editable;

    private TextView pushback;
    private TextView address;
    private TextView push;

    private EditText pushphone;
    private EditText pushtitle;
    private EditText pushcontent;

    private Spinner addkind;
    private LinearLayout pushadd;

    private RadioGroup pushtype;
    private RadioButton push1;
    private RadioButton push2;

    private ImageView openimg;


    private PushInfo pushInfo = new PushInfo();
    private int p;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Permissions.verifyStoragePermissions(this);


        initview();
        addkind = findViewById(R.id.addkind);
        initSpinner();
        addkind.setOnItemSelectedListener(new SpinnerItemListener());
    }

    private void initview() {
        pushback = findViewById(R.id.pushback);
        address = findViewById(R.id.address);

        push = findViewById(R.id.push);
        pushphone = findViewById(R.id.pushphone);
        pushtitle = findViewById(R.id.pushtitle);
        pushcontent = findViewById(R.id.pushcontent);
        pushadd = findViewById(R.id.pushadd);
        push1 = findViewById(R.id.push1);
        push2 = findViewById(R.id.push2);
        openimg = findViewById(R.id.openimg);
        pushtype = findViewById(R.id.pushtype);

        openimg.setOnClickListener(this);
        pushback.setOnClickListener(this);
        push.setOnClickListener(this);
        pushadd.setOnClickListener(this);




    }

    private void initSpinner() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Message message = new Message();
                    String str = HttpClientUtils.HttpClientGet(rootUrl+"type/getType",null);
                    JSONObject jsonObj = JSONObject.parseObject(str);
                    String b = jsonObj.get("data").toString();
                    message.what = 101;
                    message.obj = b;
                    handler.sendMessage(message);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }

    public class SpinnerItemListener implements android.widget.AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String selected = parent.getItemAtPosition(position).toString();
             p = position + 1;

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.pushback:
                //返回MainActivity
                Intent intent = new Intent();
                setResult(103, intent);
                finish();
                break;
            case R.id.push:
                if (TextUtils.isEmpty(pushtitle.getText()) ||TextUtils.isEmpty(pushcontent.getText())){
                    Toast.makeText(getApplicationContext(),"标题正文不能为空",Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(address.getText())){
                    Toast.makeText(getApplicationContext(),"请选择位置",Toast.LENGTH_SHORT).show();
                }

                else{
                    DialogUtils.dialog(this,"确定发布帖子无误了吗?","确认",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    publishNews();
                                }
                            });
                }
                break;
            case R.id.openimg:
                openImage();
                break;
            case R.id.pushadd:
                Intent addintent = new Intent(this, BaiduMapActivity.class);
                startActivityForResult(addintent,910);
                break;
        }
    }


    private void openImage() {
        //打开图库
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent,REQUEST_CODE_SYS_IMG);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data==null){
            return;
        }
        else{
            switch (requestCode){
                case 910:
                    if (resultCode == 911){
                        address.setText(data.getStringExtra("mapmsg"));

                    }
                    break;
                case REQUEST_CODE_SYS_IMG&RESULT_OK:
                    String path = HandleOSImagePath.ImagePath(this, data);
                    String filefix = path.substring(path.lastIndexOf("."));
                    String fileName = getFileName()+filefix;
                    rfileMap.put(fileName,path);
                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    //添加图片
                    if (bitmap!=null){
                        insertBitmap(bitmap,fileName);
                    }
                    break;
            }
        }


    }

    private String getFileName(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }

    private void insertBitmap(Bitmap bitmap,String fileName) {
        String bmpTag="<img src="+fileName+"/>";

        int etwidth = pushcontent.getWidth();
        Bitmap zoombmp=zoomBitmap(bitmap,etwidth);
        SpannableString spannableString = new SpannableString(bmpTag);
        ImageSpan imageSpan = new ImageSpan(this,zoombmp);
        spannableString.setSpan(imageSpan,0,bmpTag.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (editable==null){
            editable=pushcontent.getText();
        }
        editable = pushcontent.getText();
        int start=pushcontent.getSelectionStart();
        start=start>editable.length()?editable.length():start;
        editable.insert(start,spannableString);
        pushcontent.setText(editable);
    }
    private Bitmap zoomBitmap(Bitmap bitmap, int etwidth) {
        int bmpWidth = bitmap.getWidth();
        int bmpHeight = bitmap.getHeight();
        Matrix matrix = new Matrix();
        float scale =(float) etwidth/bmpWidth/3;
        matrix.setScale(scale,scale);
        Bitmap bmp = Bitmap.createBitmap(bitmap,0,0,bmpWidth,bmpHeight,matrix,true);
        return bmp;
    }


    private void publishNews() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String content = pushcontent.getText().toString();
                Log.i("AAA","aaa");
                Map<String,File>fileMap=findUplaodImg(content);
                try{
                    User user = new User();
                    //获取SharedPreferences储存的数据
                    SharedPreferences sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
                    user.setPhone(sharedPreferences.getString("phone",""));
                    Map<String,String>userMap=new HashMap<>();
                    userMap.put("dic",user.getPhone());
                    HttpClientUtils.HttpMultipartPost(rootUrl+"pushInfo/multipartAction",userMap,fileMap);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private Map<String, File> findUplaodImg(String content) {

        String imgTag = "<img src=([\\\\u4E00-\\\\u9FA5A-Za-z0-9%_/.]*)/>";
        Pattern pattern = Pattern.compile(imgTag);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            String imgName = matcher.group(1);
            String path = rfileMap.get(imgName).toString();
            Log.i("AAA",path);
            fileMap.put(imgName,new File(path));
        }
        publishNewsModel();
        return fileMap;
    }



    private void publishNewsModel() {
        final Map<String, Object> map = new HashMap<>();
        User user = new User();
        SharedPreferences sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
        user.setUserId(sharedPreferences.getLong("user_id",0));
        Type type = new Type();
        type.setTypeId(p);
        pushInfo.setUser(user);
        pushInfo.setTitle(pushtitle.getText().toString());
        pushInfo.setContent(pushcontent.getText().toString());
        pushInfo.setAddress(address.getText().toString());
        pushInfo.setPhone(pushphone.getText().toString());
        pushInfo.setStatus(2);
        pushInfo.setLikeNumber(0);
        pushInfo.setType(type);
        for (int i = 0; i<pushtype.getChildCount(); i++){
            RadioButton childAt = (RadioButton)pushtype.getChildAt(i);
            if (childAt.isChecked()){
                String str = childAt.getText().toString();
                if (str.equals("寻找失物")){
                    pushInfo.setLastType(0);
                }
                else {
                    pushInfo.setLastType(1);
                }
            }


        }


        final String string = JSON.toJSONString(pushInfo);
        map.put("adddata",string);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Message message = new Message();
                    String s = HttpClientUtils.HttpClientPost(rootUrl + "pushInfo/addpushInfo", map);
                    message.what = 555;
                    message.obj = s;
                    handler.sendMessage(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();



    }


    private Handler handler= new Handler(){
        @Override
        public void dispatchMessage(@NonNull Message msg) {
            super.dispatchMessage(msg);
            if (msg.what == 101){
                String str = msg.obj.toString();
                list = JSON.parseArray(str, Type.class);
                System.out.println(list);
                ArrayList data_list = new ArrayList<>();
                for(int i=0;i<list.size();i++){
                    data_list.add(list.get(i).getName());
                }
                ArrayAdapter arr_adapter = new ArrayAdapter<>(PublishActivity.this, android.R.layout.simple_spinner_dropdown_item, data_list);
                arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                addkind.setAdapter(arr_adapter);



            }

            if (msg.what == 555){
                String str = msg.obj.toString();
                Result result= JSON.parseObject(str, Result.class);
                String str2 = result.getMsg();
                int code = result.getCode();
                if (code==1){
                    Toast.makeText(getApplicationContext(),str2+"，请等待审核",Toast.LENGTH_SHORT).show();
                    Intent pushintent = new Intent();
                    setResult(102, pushintent);
                    finish();
                }
                else {
                    Toast.makeText(getApplicationContext(),str2+"，请检查网络",Toast.LENGTH_SHORT).show();
                }
            }
        }
    };



}
