package com.example.lostproperty.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.model.User;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.LoadingDialog;
import com.example.lostproperty.utils.MD5Utils;
import com.example.lostproperty.utils.SharedPreferencesUtils;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class ChangePWDActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etOldPWD, etCangePWD1, etCangePWD2;
    private Button btnChangePWD;
    public TextView tvback;
    private LoadingDialog mLoadingDialog; //显示正在加载的对话框
    private User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepwd);
        initViews();
        setupEvents();
        initDate();
    }

    private void initDate() {
        SharedPreferences spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        user.setUserId(spUser.getLong("user_id", 0));
        user.setUserName(spUser.getString("username", ""));
        user.setGender(spUser.getInt("gender", 0));
        user.setNickName(spUser.getString("nickname", ""));
        user.setPhone(spUser.getString("phone", ""));
        user.setPassword(spUser.getString("password", ""));
        user.setAvatar(spUser.getString("avatar", ""));
        user.setCreatedDate(spUser.getString("createddate", ""));
    }

    private void initViews() {
        etOldPWD = findViewById(R.id.et_oldpwd);
        etCangePWD1 = findViewById(R.id.et_changepwd1);
        etCangePWD2 = findViewById(R.id.et_changepwd2);
        btnChangePWD = findViewById(R.id.btn_changepwd);
        tvback = findViewById(R.id.tv_cp_back);
    }

    private void setupEvents() {
        etOldPWD.setOnClickListener(this);
        etCangePWD1.setOnClickListener(this);
        etCangePWD2.setOnClickListener(this);
        btnChangePWD.setOnClickListener(this);
        tvback.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_changepwd:
                changePassword();
                break;
            case R.id.tv_cp_back:
                Intent intent = new Intent(this,MainActivity.class);
                intent.putExtra("id",1);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void changePassword() {
        if (getPassword1().isEmpty()) {
            showToast("你输入的密码为空！");
            return;
        }
        if (getPassword2().isEmpty()) {
            showToast("请再次输入密码！");
            return;
        }
        if (!getPassword2().equals(getPassword1())) {
            showToast("两次输入密码不一致！");
            return;
        }
        if (getOldPassword().equals(getPassword1())) {
            showToast("密码未更改！");
            return;
        }
        if (!MD5Utils.getMD5Code(getOldPassword()).equals(user.getPassword())) {
            showToast("原密码错误！");
            return;
        }
        showLoading();//显示加载框
        setChangePWDBtnClickable(false);//点击按钮后，设置按钮不可点击状态
        Thread changePasswordRunnable = new Thread() {
            @Override
            public void run() {
                super.run();

                //睡眠1秒
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                user.setPassword(MD5Utils.getMD5Code(getPassword1()));
                getChangePasswordThread();
            }
        };
        changePasswordRunnable.start();

    }

    private void getChangePasswordThread() {
        new Thread(new Runnable() {

            @Override
            public void run() {

                Gson gson = new Gson();
                Message message = new Message();
                String resultJson = null;
                Map<String, Object> map = new HashMap<>();
                final String string = JSON.toJSONString(user);
                map.put("upuser", string);
                try {
                    resultJson = HttpClientUtils.HttpClientPost(IpConfig.ROOTURL_YEN_LUY + "user/updateUser", map);

                    Result result = gson.fromJson(resultJson, Result.class);
                    if (result.getCode() == 0) {
                        message.what = RESULT_OK;

                    } else {
                        message.what = RESULT_CANCELED;
                    }
                    handler.handleMessage(message);
                } catch (Exception e) {
                    showToast("网络请求失败！");
                    e.printStackTrace();
                }
                setChangePWDBtnClickable(true);  //可按更改按钮
                hideLoading();//隐藏加载框

            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case RESULT_OK:
                    showToast("密码修改成功,请重新登录");
                    reLogin();
                    break;
                case RESULT_CANCELED:
                    showToast("网络出错请重新尝试");
                    break;
            }
        }
    };

    /**
     * 弹窗工具
     */
    public void showLoading() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this, "正在加载数据", false);
        }
        mLoadingDialog.show();
    }

    /**
     * 是否可以点击登录按钮
     */
    public void setChangePWDBtnClickable(boolean clickable) {
        btnChangePWD.setClickable(clickable);
    }

    /**
     * 获取密码1
     */
    private String getPassword1() {
        return etCangePWD1.getText().toString().trim();//去掉空格
    }

    /**
     * 获取密码2
     */
    private String getPassword2() {
        return etCangePWD2.getText().toString().trim();//去掉空格
    }

    /**
     * 获取原密码
     */
    private String getOldPassword() {
        return etOldPWD.getText().toString().trim();//去掉空格

    }

    /**
     * 监听回退键
     */
    @Override
    public void onBackPressed() {
        if (mLoadingDialog != null) {
            if (mLoadingDialog.isShowing()) {
                mLoadingDialog.cancel();
            } else {
                finish();
            }
        } else {
            finish();
        }

    }

    /**
     * 页面销毁前回调的方法
     */
    protected void onDestroy() {
        if (mLoadingDialog != null) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
        super.onDestroy();
    }

    /**
     * 弹窗工具
     */
    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ChangePWDActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });

    }

    /**
     * 重新登录
     */
    private void reLogin() {
        SharedPreferences spUser = getSharedPreferences("user", Context.MODE_PRIVATE);
        spUser.edit().clear().commit();
        SharedPreferencesUtils helper = new SharedPreferencesUtils(this, "setting");
        helper.putValues(new SharedPreferencesUtils.ContentValue("autoLogin", false));
        Intent intent = new Intent(this, UserLoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * 隐藏加载的进度框
     */
    public void hideLoading() {
        if (mLoadingDialog != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLoadingDialog.hide();
                }
            });

        }
    }
}
