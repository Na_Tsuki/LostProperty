package com.example.lostproperty.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.adapter.PushInfoAdapter;
import com.example.lostproperty.model.PushInfo;
import com.example.lostproperty.utils.Constant;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.ListLoadUtil;

import java.util.ArrayList;
import java.util.List;

public class MyLoveActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout myLoveTop;
    private ImageView collectBack;
    private ListLoadUtil myLoveList;
    private ProgressBar progressBar;
    private long pushInfoId = 0;
    private long userId = 0;
    public static int start = 0;
    public static boolean isRefresh = false;

    List<PushInfo> list = null;
    List<PushInfo> sumList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_love);

        //获取手机本地存储
        SharedPreferences sharedPreferences = this.getSharedPreferences("user",this.MODE_PRIVATE);
        userId = sharedPreferences.getLong("user_id",0);

        initView();
        myLoveListThread();
        myLoveListOnClick();
        myLoveListLoad();
    }

    private void initView() {
        myLoveTop = findViewById(R.id.myLoveTop);
        collectBack = findViewById(R.id.collectBack);
        myLoveList = findViewById(R.id.myLoveList);

        myLoveTop.setOnClickListener(this);
        collectBack.setOnClickListener(this);
    }

    private void myLoveListThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = IpConfig.duzq +"collect/getCollect/"+userId+"/"+start;
                Message message = new Message();
                try {
                    String str = HttpClientUtils.HttpClientGet(url,null);
                    JSONObject jsonObject = JSONObject.parseObject(str);
                    str = jsonObject.get("data").toString();
                    message.what = Constant.GET_MYLOVE_MESSAGE_SUCCESS;
                    message.obj = str;
                    handler.sendMessage(message);
                } catch (Exception e) {
                    message.what = Constant.GET_REQUEST_FAIL;
                    handler.sendMessage(message);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void myLoveListLoad() {
        //列表上拉下拉操作
        myLoveList.setonRefreshListener(new ListLoadUtil.onRefreshListener() {
            //下拉操作
            @Override
            public void refresh() {
                new  Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        start = 0;
                        isRefresh = true;
                        myLoveList.setOnRefreshComplete();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String url = IpConfig.duzq +"collect/getCollect/"+userId+"/"+start;
                                Message message = new Message();
                                try {
                                    String str = HttpClientUtils.HttpClientPost(url,null);
                                    JSONObject jsonObject = JSONObject.parseObject(str);
                                    str = jsonObject.get("data").toString();
                                    message.what = Constant.GET_MYLOVE_MESSAGE_SUCCESS;
                                    message.obj = str;
                                    handler.sendMessage(message);
                                } catch (Exception e) {
                                    message.what = Constant.GET_REQUEST_FAIL;
                                    handler.sendMessage(message);
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }.sendEmptyMessageDelayed(0,3000);
            }

            //上拉操作
            @Override
            public void loadingMore() {
                new  Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        start = start+1;
                        isRefresh = false;
                        myLoveList.setOnRefreshComplete();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String url = IpConfig.duzq +"collect/getCollect/"+userId+"/"+start;
                                Message message = new Message();
                                try {
                                    String str = HttpClientUtils.HttpClientPost(url,null);
                                    JSONObject jsonObject = JSONObject.parseObject(str);
                                    str = jsonObject.get("data").toString();
                                    message.what = Constant.GET_MYLOVE_MESSAGE_SUCCESS;
                                    message.obj = str;
                                    handler.sendMessage(message);
                                } catch (Exception e) {
                                    message.what = Constant.GET_REQUEST_FAIL;
                                    handler.sendMessage(message);
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }.sendEmptyMessageDelayed(0,3000);
            }
        });
    }

    public void myLoveListOnClick(){
        //详情操作
        myLoveList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pushInfoId=sumList.get(position-1).getPushInfoId();
                Intent intent = new Intent(MyLoveActivity.this, PropertyActivity.class);
                intent.putExtra("pushInfoId",pushInfoId);
                startActivityForResult(intent,Constant.PUSHINFO_CONTENT_MESSAGE_SUCCESS);
            }
        });
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            progressBar = findViewById(R.id.myLovePB);
            progressBar.setVisibility(View.GONE);
            switch (msg.what){
                //收藏列表
                case Constant.GET_MYLOVE_MESSAGE_SUCCESS:
                    String str = msg.obj.toString();
                    if (str.equals("[]")){
                        Toast.makeText(MyLoveActivity.this, "暂无收藏!", Toast.LENGTH_SHORT).show();
                    }else if(str.equals("noMore")){
                        Toast.makeText(MyLoveActivity.this, "暂无更多收藏!", Toast.LENGTH_SHORT).show();
                    }else{
                        list = JSONObject.parseArray(str, PushInfo.class);
                        if (isRefresh){
                            sumList.clear();
                        }
                        sumList.addAll(list);
                        final PushInfoAdapter pushInfoAdapter = new PushInfoAdapter(MyLoveActivity.this,sumList);
                        if (start != 0){
                            myLoveList.invalidateViews();
                            pushInfoAdapter.notifyDataSetChanged();
                        }else{
                            myLoveList.setAdapter(pushInfoAdapter);
                        }
                    }
                    break;
                //请求出错
                case Constant.GET_REQUEST_FAIL:
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.collectBack:
                back();
                break;
            case R.id.myLoveTop:
                myLoveList.smoothScrollToPosition(0);
                break;
        }
    }

    private void back() {
        start = 0;
        Intent intent = new Intent();
        setResult(Constant.MYLOVE_RESULT_MESSAGE,intent);
        finish();
    }
}
