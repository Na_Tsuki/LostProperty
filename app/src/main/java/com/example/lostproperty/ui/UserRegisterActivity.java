package com.example.lostproperty.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.model.Result;
import com.example.lostproperty.model.User;
import com.example.lostproperty.utils.Base64Utils;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.LoadingDialog;
import com.example.lostproperty.utils.MD5Utils;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class UserRegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tv_back;
    private EditText et_phoneNumber;
    private EditText et_password1;
    private EditText et_password2;
    private Button btn_register;
    private LoadingDialog mLoadingDialog; //对话框

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initViews();
        setupEvents();

    }

    private void initViews() {
        tv_back = findViewById(R.id.tv_r_back);
        et_phoneNumber = findViewById(R.id.et_r_phoneNumber);
        et_password1 = findViewById(R.id.et_r_password1);
        et_password2 = findViewById(R.id.et_r_password2);
        btn_register = findViewById(R.id.btn_register);

    }

    private void setupEvents() {
        tv_back.setOnClickListener(this);
        btn_register.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_r_back:
                Intent intent = new Intent(this,UserLoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_register:
                register();
                break;

        }
    }

    private void register() {

        if (!isPhoneNumber(getAccount())) {
            showToast("请输入正确手机号！");
            return;
        }
        if (getAccount().isEmpty()) {
            showToast("你输入的手机号为空！");
            return;
        }

        if (getPassword1().isEmpty()) {
            showToast("你输入的密码为空！");
            return;
        }
        if (getPassword2().isEmpty()) {
            showToast("请再次输入密码！");
            return;
        }
        if (!getPassword2().equals(getPassword1())) {
            showToast("两次输入密码不一致！");
            return;
        }
        showLoading();//显示加载框
        setRegisterBtnClickable(false);//点击注册后，设置注册按钮不可点击状态
        Thread registerRunnable = new Thread() {
            @Override
            public void run() {
                super.run();
                //睡眠1秒
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //写入数据
                getRegisterThread();

            }


        };
        registerRunnable.start();


    }


    private void getRegisterThread() {
        new Thread(new Runnable() {
            @Override

            public void run() {

                if (cheackUser()) {
                Message message = new Message();
                    Map<String, Object> map = new HashMap<>();
                    User user = new User();
                user.setUserName(getAccount());
                user.setNickName("拾忆用户" + getAccount());
                user.setPhone(getAccount());
                user.setAvatar("123");
                user.setPassword(MD5Utils.getMD5Code(getPassword1()));
                user.setGender(0);
                final String string = JSON.toJSONString(user);
                map.put("adduser", string);
                String resultJson = null;

                    try {
                        resultJson = HttpClientUtils.HttpClientPost(IpConfig.ROOTURL_YEN_LUY + "user/addUser", map);
                        Result result = Result.toResult(resultJson);
                        if (result.getCode() == 1) {
                            message.what = RESULT_OK;
                        } else {
                            message.what = RESULT_CANCELED;
                        }
                        handler.handleMessage(message);
                    } catch (Exception e) {
                        showToast("网络请求失败！");
                        e.printStackTrace();
                    }
                } else {
                    showToast("用户已存在！");
                }
                setRegisterBtnClickable(true);  //可按注册按钮
                hideLoading();//隐藏加载框

            }
        }).start();

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case RESULT_OK:
                    showToast("注册成功");
                    registerOK();
                    break;
                case RESULT_CANCELED:
                    showToast("注册失败");
                    break;
            }
        }
    };

    private void registerOK() {
        Intent intent = new Intent(this, UserLoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * 隐藏加载的进度框
     */
    public void hideLoading() {
        if (mLoadingDialog != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLoadingDialog.hide();
                }
            });

        }
    }

    /**
     * 是否可以点击注册按钮
     */
    public void setRegisterBtnClickable(boolean clickable) {
        btn_register.setClickable(clickable);
    }

    /**
     * 显示加载的进度框
     */
    public void showLoading() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this, "正在加载数据", false);
        }
        mLoadingDialog.show();
    }

    private String getPassword1() {
        return et_password1.getText().toString().trim();//去掉空格
    }

    private String getPassword2() {
        return et_password2.getText().toString().trim();//去掉空格
    }

    private String getAccount() {
        return et_phoneNumber.getText().toString().trim();//去掉空格

    }

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(UserRegisterActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });

    }


    public boolean cheackUser() {
        Gson gson = new Gson();
        Message message = new Message();
        String userName = getAccount();
        String resultJson = null;
        Map<String, Object> map = new HashMap<>();
        map.put("userName", userName);
        try {
            resultJson = HttpClientUtils.HttpClientPost(IpConfig.ROOTURL_YEN_LUY + "user/getUserByName", map);
            Result result = gson.fromJson(resultJson, Result.class);
            if (result.getCode() == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            showToast("网络请求失败！");
            e.printStackTrace();
        }
        return true;
    }
    /**
     * 监听回退键
     */
    @Override
    public void onBackPressed() {
        if (mLoadingDialog != null) {
            if (mLoadingDialog.isShowing()) {
                mLoadingDialog.cancel();
            } else {
                finish();
            }
        } else {
            finish();
        }

    }


    /**
     * 页面销毁前回调的方法
     */
    protected void onDestroy() {
        if (mLoadingDialog != null) {
            mLoadingDialog.cancel();
            mLoadingDialog = null;
        }
        super.onDestroy();
    }

    /**
     * 判断号码
     */
    public static boolean isPhoneNumber(String input) {// 判断手机号码是否规则
        String regex = "(1[0-9][0-9]|15[0-9]|18[0-9])\\d{8}";
        Pattern p = Pattern.compile(regex);
        return p.matches(regex, input);//如果不是号码，则返回false，是号码则返回true

    }
}
