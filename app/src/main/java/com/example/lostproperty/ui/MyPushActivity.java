package com.example.lostproperty.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.example.lostproperty.IpConfig;
import com.example.lostproperty.R;
import com.example.lostproperty.adapter.PushInfoAdapter;
import com.example.lostproperty.model.PushInfo;
import com.example.lostproperty.utils.Constant;
import com.example.lostproperty.utils.DialogUtils;
import com.example.lostproperty.utils.HttpClientUtils;
import com.example.lostproperty.utils.ListLoadUtil;

import java.util.ArrayList;
import java.util.List;

public class MyPushActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout myPushTop;
    private ImageView pushBack;
    private ListLoadUtil myPushList;
    private ProgressBar progressBar;

    private long pushInfoId = 0;
    private long userId = 0;
    public static int start = 0;
    public static boolean isRefresh = false;

    List<PushInfo> list = null;
    List<PushInfo> sumList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_push);

        //获取手机本地存储
        SharedPreferences sharedPreferences = this.getSharedPreferences("user",this.MODE_PRIVATE);
        userId = sharedPreferences.getLong("user_id",0);

        initView();
        myPushListThread();
        myPushListOnClick();
        myPushListLongClick();
        myPushListLoad();
    }

    private void initView() {
        myPushTop = findViewById(R.id.myPushTop);
        pushBack = findViewById(R.id.pushBack);
        myPushList = findViewById(R.id.myPushList);

        myPushTop.setOnClickListener(this);
        pushBack.setOnClickListener(this);
    }

    private void myPushListThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = IpConfig.duzq +"pushInfo/getUserPushInfo/"+userId+"/"+start;
                Message message = new Message();
                try {
                    String str = HttpClientUtils.HttpClientPost(url,null);
                    JSONObject jsonObject = JSONObject.parseObject(str);
                    str = jsonObject.get("data").toString();
                    message.what = Constant.GET_MYPUSH_MESSAGE_SUCCESS;
                    message.obj = str;
                    handler.sendMessage(message);
                } catch (Exception e) {
                    message.what = Constant.GET_REQUEST_FAIL;
                    handler.sendMessage(message);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void myPushListLoad() {
        //列表上拉下拉操作
        myPushList.setonRefreshListener(new ListLoadUtil.onRefreshListener() {
            //下拉操作
            @Override
            public void refresh() {
                    new  Handler(){
                        @Override
                        public void handleMessage(Message msg) {
                            start = 0;
                            isRefresh = true;
                            myPushList.setOnRefreshComplete();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String url = IpConfig.duzq +"pushInfo/getUserPushInfo/"+userId+"/"+start;
                                    Message message = new Message();
                                    try {
                                        String str = HttpClientUtils.HttpClientPost(url,null);
                                        JSONObject jsonObject = JSONObject.parseObject(str);
                                        str = jsonObject.get("data").toString();
                                        message.what = Constant.GET_MYPUSH_MESSAGE_SUCCESS;
                                        message.obj = str;
                                        handler.sendMessage(message);
                                    } catch (Exception e) {
                                        message.what = Constant.GET_REQUEST_FAIL;
                                        handler.sendMessage(message);
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    }.sendEmptyMessageDelayed(0,3000);
            }

            //上拉操作
            @Override
            public void loadingMore() {
                    new  Handler(){
                        @Override
                        public void handleMessage(Message msg) {
                            start = start+1;
                            isRefresh = false;
                            myPushList.setOnRefreshComplete();
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String url = IpConfig.duzq +"pushInfo/getUserPushInfo/"+userId+"/"+start;
                                    Message message = new Message();
                                    try {
                                        String str = HttpClientUtils.HttpClientPost(url,null);
                                        JSONObject jsonObject = JSONObject.parseObject(str);
                                        str = jsonObject.get("data").toString();
                                        message.what = Constant.GET_MYPUSH_MESSAGE_SUCCESS;
                                        message.obj = str;
                                        handler.sendMessage(message);
                                    } catch (Exception e) {
                                        message.what = Constant.GET_REQUEST_FAIL;
                                        handler.sendMessage(message);
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    }.sendEmptyMessageDelayed(0,3000);
                }
        });
    }

    public void myPushListOnClick(){
        //详情操作
        myPushList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pushInfoId=sumList.get(position-1).getPushInfoId();
                Intent intent = new Intent(MyPushActivity.this, PropertyActivity.class);
                intent.putExtra("pushInfoId",pushInfoId);
                startActivityForResult(intent,Constant.PUSHINFO_CONTENT_MESSAGE_SUCCESS);
            }
        });
    }

    private void myPushListLongClick() {
        //删除操作
        myPushList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                DialogUtils.dialog(MyPushActivity.this,"确认删除吗?","删除", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pushInfoId = sumList.get(position - 1).getPushInfoId();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                String url = IpConfig.duzq+"pushInfo/deletePushById/"+pushInfoId;
                                Message message = new Message();
                                try {
                                    String str = HttpClientUtils.HttpClientPost(url, null);
                                    JSONObject jsonObject = JSONObject.parseObject(str);
                                    str = jsonObject.get("data").toString();
                                    message.what = Constant.PUSHINFO_DELETE_MESSAGE_SUCCESS;
                                    message.obj = str;
                                    handler.sendMessage(message);
                                } catch (Exception e) {
                                    message.what = Constant.GET_REQUEST_FAIL;
                                    handler.sendMessage(message);
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                });
                return true;
            }
        });
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            progressBar = findViewById(R.id.myPushPB);
            progressBar.setVisibility(View.GONE);
            switch (msg.what){
                //帖子列表
                case Constant.GET_MYPUSH_MESSAGE_SUCCESS:
                    String str = msg.obj.toString();
                    if (str.equals("[]")){
                        Toast.makeText(MyPushActivity.this, "暂无帖子!", Toast.LENGTH_SHORT).show();
                    }else if(str.equals("noMore")){
                        Toast.makeText(MyPushActivity.this, "暂无更多帖子!", Toast.LENGTH_SHORT).show();
                    }else{
                        list = JSONObject.parseArray(str, PushInfo.class);
                        if (isRefresh){
                            sumList.clear();
                        }
                        sumList.addAll(list);
                        final PushInfoAdapter pushInfoAdapter = new PushInfoAdapter(MyPushActivity.this,sumList);
                        if (start != 0){
                            myPushList.invalidateViews();
                            pushInfoAdapter.notifyDataSetChanged();
                        }else{
                            myPushList.setAdapter(pushInfoAdapter);
                        }
                    }
                    break;
                 //删除
                case Constant.PUSHINFO_DELETE_MESSAGE_SUCCESS:
                    String string = msg.obj.toString();
                    if (string.equals("0")){
                        Toast.makeText(MyPushActivity.this, "删除成功!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(MyPushActivity.this, "删除失败,请重试!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                //请求出错
                case Constant.GET_REQUEST_FAIL:
                    Toast.makeText(MyPushActivity.this, "请求出错!", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pushBack:
                back();
                break;
            case R.id.myPushTop:
                myPushList.smoothScrollToPosition(0);
                break;
        }
    }

    private void back() {
        start = 0;
        Intent intent = new Intent();
        setResult(Constant.MYPUSH_RESULT_MESSAGE,intent);
        finish();
    }
}
