package com.example.lostproperty.enumeration;

import java.util.Arrays;

public enum Day {
    // 记住最后要用分号结束
    MONDAY("星期一",1),
    TUESDAY("星期二",2);

    private String desc;//文字描述
    private Integer code; //对应的代码

    /**
     * 私有构造,防止被外部调用
     * @param desc code
     */
    private Day(String desc,Integer code){
        this.desc=desc;
        this.code=code;
    }
    /**
     * 定义方法,返回描述,跟常规类的定义没区别
     * @return desc
     */
    public String getDesc(){
        return desc;
    }

    /**
     * 定义方法,返回代码,跟常规类的定义没区别
     * @return code
     */
    public int getCode(){
        return code;
    }


}