package com.example.lostproperty.enumeration;

import java.util.HashMap;
import java.util.Map;

public class EnumMap {
    public static String show(Integer code) {
        Map<Integer, String> map = new HashMap<Integer, String>();
        //给map中添加元素
        map.put(Day.MONDAY.getCode(), Day.MONDAY.getDesc());
        return map.get(code);
    }
}

